_default:
  @just --unstable --list

lint:
  #!/usr/bin/env bash
  echo "Linting files..."
  set -euo pipefail
  shopt -s globstar
  nix fmt **/*.nix
  stylua .
  shellcheck **/*.sh

add: lint
  git add -A

switch *OPTS: add
  sudo nixos-rebuild --flake . switch {{OPTS}}

boot *OPTS: add
  sudo nixos-rebuild --flake . boot {{OPTS}}

build *OPTS: add
  nixos-rebuild --flake . build {{OPTS}}

test *OPTS: add
  sudo nixos-rebuild --flake . test {{OPTS}}

update *OPTS:
  nix flake update --commit-lock-file {{OPTS}}

upgrade: update switch

garbage-collect:
	nix-collect-garbage
	docker system prune --volumes --all -f

detect-garbage-roots:
	nix-store --gc --print-roots | egrep -v "^(/nix/var|/run/\w+-system|\{memory|/proc)"
