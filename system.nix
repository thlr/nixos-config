{
  system,
  inputs,
  pkgs,
  username,
  hostname,
  projectRoot,
}:
{
  inherit system;

  specialArgs = {
    inherit username hostname;
  };

  modules = [
    {
      nixpkgs = {
        inherit pkgs;
      };
    }

    (projectRoot + "/systems/${hostname}/nixos")
    (projectRoot + "/systems/${hostname}/config.nix")
    (projectRoot + /modules/nixos)

    # Home Manager configuration
    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        backupFileExtension = "homemanager.backup";
        useGlobalPkgs = true;
        useUserPackages = true;
        users.${username} = {
          imports = [
            (projectRoot + "/systems/${hostname}/home")
            (projectRoot + "/systems/${hostname}/config.nix")
            (projectRoot + /modules/home)
          ];
          home = {
            inherit username;
            homeDirectory = "/home/${username}";
          };
        };
        extraSpecialArgs = {
          inherit inputs;
          inherit system;
        };
      };
    }
  ];
}
