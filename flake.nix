{
  description = "Theo Larue's NixOS configuration";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "nixpkgs/nixpkgs-unstable";

    git-hooks = {
      url = "github:cachix/git-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprland = {
      url = "github:hyprwm/Hyprland";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { self, ... }@inputs:
    let
      system = "x86_64-linux";
      nixPkgsConfig = {
        allowUnfree = true;
      };
      unstable = import inputs.nixpkgs-unstable {
        inherit system;
        config = nixPkgsConfig;
      };

      overlays = [
        # Unstable packages
        (_: _: { inherit unstable; })
        (final: prev: { lib = prev.lib // (import ./libExtend.nix { lib = prev.lib; }); })
      ];

      pkgs = import inputs.nixpkgs {
        inherit system overlays;
        config = nixPkgsConfig;
      };

      lib = inputs.nixpkgs.lib;

      pre-commit-checks = inputs.git-hooks.lib.${system}.run {
        src = ./.;
        hooks = {
          nixfmt-rfc-style.enable = true;
          stylua.enable = true;
          shellcheck.enable = true;
        };
      };
    in
    {
      formatter.${system} = pkgs.nixfmt-rfc-style;
      devShells.${system}.default = pkgs.mkShell {
        buildInputs =
          with pkgs;
          [
            just
            lua
            # Language servers
            nil
            sumneko-lua-language-server
            (python311.withPackages (ps: with ps; [ python-lsp-server ]))
            nodePackages.bash-language-server
          ]
          ++ pre-commit-checks.enabledPackages;
        inherit (pre-commit-checks) shellHook;
      };
      nixosConfigurations = builtins.listToAttrs (
        map
          (
            { hostname, username }:
            lib.nameValuePair hostname (
              lib.nixosSystem (
                import ./system.nix {
                  inherit
                    inputs
                    system
                    pkgs
                    username
                    hostname
                    ;
                  projectRoot = ./.;
                }
              )
            )
          )
          [
            {
              hostname = "lancey-tlarue";
              username = "tlarue";
            }
            {
              hostname = "lenovo-laptop";
              username = "thlr";
            }
          ]
      );
    };
}
