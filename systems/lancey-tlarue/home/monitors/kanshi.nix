{ config, lib, ... }:
with lib;
let
  cfg = config.thlr;
  internal = {
    description = "eDP-1";
    #description = "Sharp Corporation 0x14F7";
  };
  monitorLeftOffice = {
    description = "Iiyama North America PL2493H 1175011110118";
    #description = "DP-5";
  };
  monitorRightOffice = {
    description = "Iiyama North America PL2493H 1175001601190";
    #description = "DP-6";
  };
  officeTV = {
    description = "XXX WXGA TV 0x01010101";
  };
in
mkIf cfg.wayland.enable {
  # Detect current monitors with `hyprctl monitors`
  services.kanshi = mkMerge [
    (mkIf cfg.hyprland.enable { systemdTarget = "xdg-desktop-portal-hyprland.service"; })
    {
      enable = true;
      # "profiles" option is deprecated
      settings = [
        {
          profile = {
            name = "undocked";
            exec = "${cfg.vars.scripts.setWallpaper}";
            outputs = [
              {
                criteria = internal.description;
                status = "enable";
              }
            ];
          };
        }
        {
          profile = {
            name = "office-desk";
            exec = "${cfg.vars.scripts.setWallpaper}";
            outputs = [
              {
                criteria = internal.description;
                status = "disable";
              }
              {
                criteria = monitorLeftOffice.description;
                status = "enable";
                position = "0,0";
                mode = "1920x1080@60Hz";
              }
              {
                criteria = monitorRightOffice.description;
                status = "enable";
                position = "1920,0";
                mode = "1920x1080@60Hz";
              }
            ];
          };
        }
        {
          profile = {
            name = "meeting-table";
            exec = "${cfg.vars.scripts.setWallpaper}";
            outputs = [
              {
                criteria = internal.description;
                status = "enable";
                #scale = internal.scale;
              }
              {
                criteria = officeTV.description;
                status = "enable";
                position = "1920,0";
                mode = "1360x768@60Hz";
              }
            ];
          };
        }
      ];
    }
  ];
}
