{ lib, config, ... }:
with lib;
let
  cfg = config.thlr;
in
{
  imports = [ ./kanshi.nix ];
  config = mkMerge [
    (mkIf cfg.xorg.enable {
      home.shellAliases = {
        ac = "autorandr -c";
      };
    })
  ];
}
