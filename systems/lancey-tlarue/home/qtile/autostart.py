from libqtile import hook
import subprocess

autostart = {
    "1": "firefox",
    "8": "obsidian",
    "9": "discord"
}


@hook.subscribe.startup_once
def autostart_commands():
    """
    Other autostart commands
    """
    processes = [
        ['autorandr', '-c'],
    ]

    for p in processes:
        subprocess.Popen(p)
