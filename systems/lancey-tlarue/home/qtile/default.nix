{ lib, config, ... }:
with lib;
let
  cfg = config.thlr;
in
mkIf cfg.qtile.enable { xdg.configFile."qtile/autostart.py".source = ./autostart.py; }
