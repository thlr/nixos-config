{ pkgs, ... }:
{
  imports = [
    ./git.nix
    ./onedrive.nix
    ./lazygit.nix
    ./ssh.nix
    ./just.nix
    ./qtile
    ./monitors
  ];

  home = {
    packages = with pkgs; [
      # cli
      doctl
      influxdb2-cli
      mqttui # TUI mqtt client
      teams-for-linux
    ];
  };
}
