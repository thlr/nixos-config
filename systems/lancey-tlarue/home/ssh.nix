{ config, ... }:
{
  programs.ssh =
    let
      extraOptions = {
        "AddKeysToAgent" = "yes";
        "IdentitiesOnly" = "yes";
      };
    in
    {
      enable = true;
      matchBlocks = {
        "gitlab.lancey.fr" = {
          hostname = "gitlab.lancey.fr";
          identityFile = "${config.home.homeDirectory}/.ssh/gitlab-lancey";
          inherit extraOptions;
        };
        "gitlab.com" = {
          hostname = "gitlab.com";
          identityFile = "${config.home.homeDirectory}/.ssh/gitlab";
          inherit extraOptions;
        };
        "github.com" = {
          hostname = "github.com";
          identityFile = "${config.home.homeDirectory}/.ssh/github";
          inherit extraOptions;
        };
        "houille-blanche.lancey-sandbox.cloud" = {
          hostname = "houille-blanche.lancey-sandbox.cloud";
          identityFile = "${config.home.homeDirectory}/.ssh/hb-sandbox";
          inherit extraOptions;
        };
        "houille-blanche.lancey.cloud" = {
          hostname = "houille-blanche.lancey.cloud";
          identityFile = "${config.home.homeDirectory}/.ssh/hb-production";
          inherit extraOptions;
        };
        "registry.lancey.cloud" = {
          hostname = "registry.lancey.cloud";
          identityFile = "${config.home.homeDirectory}/.ssh/harbor";
          inherit extraOptions;
        };
        "elixir-runner.lanceylan" = {
          hostname = "elixir-runner.lanceylan";
          user = "elixir";
          extraOptions = {
            "PasswordAuthentication" = "yes";
            "PubkeyAuthentication" = "no";
          };
        };
        "server.lancey.fr" = {
          hostname = "server.lancey.fr";
          identityFile = "${config.home.homeDirectory}/.ssh/lenny";
          extraOptions = extraOptions // {
            "PasswordAuthentication" = "no";
            "PubkeyAuthentication" = "yes";
          };
        };
      };
    };
}
