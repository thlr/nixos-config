# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  pkgs,
  hostname,
  username,
  ...
}:
{
  imports = [
    ./xorg-monitors.nix
    ./hardware-configuration.nix
    ./nix.nix
  ];

  boot = {
    loader = {
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
      grub = {
        enable = true;
        device = "nodev";
        efiSupport = true;
        enableCryptodisk = true;
      };
    };

    initrd = {
      luks.devices."root" = {
        device = "/dev/disk/by-uuid/3dfbfe5b-8de3-4f49-988f-bac1c2a2e37f"; # UUID from blkid /dev/nvme0n1p2
        preLVM = true;
        keyFile = "/keyfile.bin";
        allowDiscards = true;
      };
      secrets = {
        "keyfile.bin" = "/etc/secrets/initrd/keyfile.bin";
      };
    };
  };

  # User services
  services.onedrive.enable = true;
  services.epmd.enable = true;
  virtualisation.docker.enable = true;

  # Users
  networking = {
    hostName = hostname;
    nameservers = [
      "9.9.9.9"
      "1.1.1.1"
    ];
  };
  users = {
    mutableUsers = false;
    users = {
      ${username} = {
        uid = 1000;
        isNormalUser = true;
        extraGroups = [
          "wheel"
          "docker"
        ]; # Enable ‘sudo’ for the user.
        shell = pkgs.zsh;
        hashedPassword = "$6$2Zurb7Loj9RYlJWU$aLTDje1fO9Gu6fuP3muBCB9blxHV7P.bMjBlUbFZ.ooTr5Pq5lL/UJa99YZAsvLW.Rof3WLqkcltuffcWcxzu.";
      };
      root.hashedPassword = "!";
    };
  };
}
