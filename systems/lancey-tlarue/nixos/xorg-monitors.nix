{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf cfg.xorg.enable {
  services = {
    #xserver.deviceSection = ''
    #  Option "ReprobeOutputs" "YES"
    #'';

    # Call autorandr when an event occurs (a monitor is plugged or un-plugged)
    udev.extraRules = ''
      ACTION=="change", SUBSYSTEM=="drm", RUN+="${pkgs.autorandr}/bin/autorandr -c"
    '';

    autorandr = {
      enable = true;
      profiles =
        let
          # To retrieve fingerprints, use autorandr --fingerprint
          internal = {
            name = "eDP1";
            edid = "00ffffffffffff004d10f71400000000181e0104a51d12780ede50a3544c99260f505400000001010101010101010101010101010101283c80a070b023403020360020b410000018203080a070b023403020360020b410000018000000fe0047524e5050804c513133344e31000000000002410332011200000a010a2020007f";
          };
          monitor1 = {
            name = "DP3-5";
            edid = "00ffffffffffff0026cd4861760000000b1f010380351e782afad5a755529d26105054b74f00714f818081c081009500b300d1c0d1cf023a801871382d40582c45000f282100001e000000fd00374c1e5512000a202020202020000000ff0031313735303131313130313138000000fc00504c32343933480a202020202001db02031ef14b9002030411121305141f01230907018301000065030c001000023a801871382d40582c45000f282100001e8c0ad08a20e02d10103e96000f2821000018011d007251d01e206e2855000f282100001e8c0ad090204031200c4055000f28210000182a4480a070382740302035000f282100001a0000000000000017";
          };
          monitor2 = {
            name = "DP3-6";
            edid = "00ffffffffffff0026cd4961a6040000101e0104a5351e783afad5a755529d26105054b74f00714f818081c081009500b300d1c0d1cf023a801871382d40582c45000f282100001e000000fd00374c1e5512000a202020202020000000ff0031313735303031363031313930000000fc00504c32343933480a20202020200168020318f14b9002030411121305141f012309070183010000023a801871382d40582c45000f282100001e8c0ad08a20e02d10103e96000f2821000018011d007251d01e206e2855000f282100001e8c0ad090204031200c4055000f28210000182a4480a070382740302035000f282100001a00000000000000000000000000a1";
          };
          mobScreen = {
            name = "DP3";
            edid = "00ffffffffffff00631800000101010101170103800000780ad7a5a2594a9624145054adce0001010101010101010101010101010101662150b051001b3040703600f8c22100001e0e1f008051001e3040803700f8c22100001c000000fc00575847412054560a2020202020000000fd00324c1e410b000a202020202020011802032df150900504020306071112131415161f012026090707150724830100006c030c002000001ec001010101023a801871382d40582c450070ea3100001e011d8018711c1620582c2500f4191100009e8c0ad08a20e02d10103a960058c2210000188c0ad08a20e02d10103e960020c23100001800000000000000000000d9";
          };
          postSwitch = {
            reloadQtile = "qtile cmd-obj -o cmd -f reload_config;";
            setWallpapers = "${cfg.vars.scripts.setWallpaper}; update-lock-image";
          };
        in
        {
          "docked" = {
            hooks.postswitch = postSwitch;
            fingerprint = {
              ${internal.name} = internal.edid;
              ${monitor1.name} = monitor1.edid;
              ${monitor2.name} = monitor2.edid;
            };
            config = {
              # three monitors is too much
              ${internal.name}.enable = false;
              # left monitor
              ${monitor1.name} = {
                enable = true;
                primary = true;
                position = "0x0";
                mode = "1920x1080";
                rate = "60.00";
              };
              # right monitor
              ${monitor2.name} = {
                enable = true;
                position = "1920x0";
                mode = "1920x1080";
                rate = "60.00";
                #rotate = "right";
              };
            };
          };
          "mob" = {
            hooks.postswitch = postSwitch;
            fingerprint = {
              ${internal.name} = internal.edid;
              ${mobScreen.name} = mobScreen.edid;
            };
            config = {
              ${internal.name} = {
                enable = true;
                primary = true;
                crtc = 0;
                position = "0x0";
                mode = "1920x1200";
                rate = "60.00";
              };
              ${mobScreen.name} = {
                enable = true;
                position = "1920x0";
                mode = "1920x1080";
                rate = "60.00";
              };
            };
          };
          "mobile" = {
            hooks.postswitch = postSwitch;
            fingerprint = {
              ${internal.name} = internal.edid;
            };
            config = {
              ${internal.name} = {
                enable = true;
                primary = true;
                crtc = 0;
                position = "0x0";
                mode = "1920x1200";
                rate = "60.00";
              };
            };
          };
        };
    };
  };
}
