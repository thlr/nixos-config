{ pkgs, ... }:
{
  thlr = {
    hyprland = {
      enable = true;
      autoLogin = false;
      internalMonitorIdentifier = "eDP-1";
      extraStartup = [
        "firefox"
        "obsidian"
        "keepassxc"
        "discord"
        "spotify"
      ];
    };
    mouseSpeed = "0.7";
    work.enable = true;
    firefox.searchEngine = "Kagi";
    fontName = "Ubuntu Nerd Font";
    utilities = {
      sessionLocker = "hyprlock";
      wallpaperEngine = "swww";
      idleDaemon = "hypridle";
      screenshot = "grim";
      #screenshot = "flameshot";
    };
    gtkTheme = {
      name = "Tokyonight-Light";
      package = pkgs.tokyonight-gtk-theme;
    };
  };
}
