{
  pkgs,
  hostname,
  username,
  ...
}:
{
  imports = [
    ./hardware-configuration.nix
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 3;
  boot.loader.efi.canTouchEfiVariables = true;

  # Basic configuration
  networking.hostName = hostname;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = false;
    users.${username} = {
      isNormalUser = true;
      extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
      shell = pkgs.zsh;
      hashedPassword = "$y$j9T$19B3Y6sPJob2gAKpxIkrg/$KzIBd47mX7oLG20MVpHBtyaR/vkXRE/nN2zVUmSn4bA"; # mkpassd
    };
    users.root.hashedPassword = "!";
  };
}
