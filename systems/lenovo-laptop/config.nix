{ pkgs, ... }:
{
  thlr = {
    hyprland = {
      enable = true;
      internalMonitorIdentifier = "eDP-1";
      autoLogin = true;
      extraStartup = [
        "firefox"
        "obsidian"
        "signal-desktop"
        "spotify"
      ];
    };
    firefox.searchEngine = "Kagi";
    fontName = "Ubuntu Nerd Font";
    utilities = {
      sessionLocker = "hyprlock";
      wallpaperEngine = "swww";
      idleDaemon = "hypridle";
      screenshot = "grim";
    };
    gtkTheme = {
      name = "Tokyonight-Dark";
      package = pkgs.tokyonight-gtk-theme;
    };
  };
}
