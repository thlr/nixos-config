{ config, ... }:
{
  programs.ssh =
    let
      extraOptions = {
        "AddKeysToAgent" = "yes";
        "IdentitiesOnly" = "yes";
      };
    in
    {
      enable = true;
      matchBlocks = {
        "gitlab.com" = {
          hostname = "gitlab.com";
          identityFile = "${config.home.homeDirectory}/.ssh/gitlab";
          inherit extraOptions;
        };
      };
    };
}
