{ config, lib, ... }:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.qtile.enable { xdg.configFile."qtile/autostart.py".source = ./autostart.py; };
}
