# thlr's config

## Usage

Run `just` for a quick setup and utility commands.

## Organization

NixOS config for my systems.

`modules` contain all the shared configuration between my systems. It contains two folders, `home` and `nixos` which contain respectively NixOS and home-manager configurations.

`systems` contains configuration specific to my different systems. In each system root folder we also find `nixos` and `home` folders to configure nixos and home-manager.

All my custom options live into `modules/options.nix` which is imported both in `/modules/nixos` and `modules/home`. They allow me to quickly change some broad configuration (window manager, username, font, ...) easily from the systems configuration.

As a standard, these options are often set in `systems/*/config.nix` files, however, they are sometimes by my custom modules themselves. For instance, hyprland will set the screen locker and display server itself:

```nix
config = mkIf cfg.hyprland.enable {
  thlr.wayland.enable = true;
  thlr.utilities.sessionLock = "hyprlock";
  wayland.windowManager.hyprland = {
    enable = true;
    ...
```

Note that these options are common to both *home-manager* and *NixOS* modules, which is very convenient to avoid redundant options.

The options defined under `thlr.vars` - in `./modules/home/variables.nix` are for internal use only and should not to be changed or accessed in systems configuration.

## User scripts and utilities

I call "user scripts" actions that are not supported natively by the desktop environment, such as taking a screenshot, controling volume or locking a session.

Using scripts to invoke these third party tools serves to decouple the desktop environment from these tools. This allows several DE to easily share a common set of tools or change the tools without changing the DE's configuration.

Scripts are defined under `config.thlr.vars.scripts`, for instance in hyprland's config:

```
wayland.windowManager.hyprland.settings.bind = [
  ", Print, exec, ${cfg.vars.scripts.screenshot}" # Screenshot
  ...
]
```

A script can be either a file (procedure described bellow) or a shell command:

```nix
  thlr.vars.scripts.screenshot = "flameshot gui";
```

These scripts are conditioned by options in `config.thlr.utilities`, taking the same screenshot example:

```nix
mkIf (cfg.utilities.screenshot == "flameshot") {
  thlr.vars.scripts.screenshot = "flameshot gui";
```

The `config.thlr.utilities` options are enumerables, for instance here is the session lock utility option:

```nix
utilities.sessionLock = mkOption {
  default = "";
  type = types.enum [
    "hyprlock"
    "xss-lock"
  ];
};
```

### Executable scripts location

To create a script in the form of an executable file, use `config.thlr.vars.binDirPath`, for instance:

```nix
home.file."${cfg.vars.binDirPath}/screenshot" = {
  executable = true;
  text = ''
    #! /usr/bin/env bash
    flameshot gui
  '';
}
```

And then set the variable to point to your file:

```nix
thlr.vars.scripts.sessionLock = "${cfg.vars.binDirFullPath}/screenshot";
```

You can also access the full path to the scripts folder using `config.thlr.vars.binDirFullPath`.
