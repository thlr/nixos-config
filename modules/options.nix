{ lib, ... }:
with lib;
{
  options.thlr = {
    # Window managers
    gnome.enable = mkEnableOption "gnome";
    qtile.enable = mkEnableOption "qtile";
    hyprland = {
      enable = mkEnableOption "hyprland";
      internalMonitorIdentifier = mkOption {
        type = types.str;
        example = "eDP-1 or desc:Sharp Corporation 0x14F7";
        default = "eDP-1";
      };
      extraStartup = mkOption {
        type = types.listOf types.str;
        description = "Will be concatenated to the hyprland startup script.";
        default = [ ];
        example = [
          "firefox"
          "obsidian"
        ];
      };
      autoLogin = mkOption {
        type = types.bool;
        description = "Autologin on startup";
        default = false;
      };
    };

    utilities = {
      screenshot = mkOption {
        default = "flameshot";
        type = types.enum [
          "flameshot"
          "grim"
        ];
      };
      sessionLocker = mkOption {
        type = types.enum [
          "hyprlock"
          "xss-lock"
        ];
      };
      wallpaperEngine = mkOption {
        type = types.enum [
          "feh"
          "swww"
          "hyprpaper"
        ];
      };
      idleDaemon = mkOption {
        type = types.enum [
          "hypridle"
        ];
      };
    };

    # Display servers. These should be enabled by the window managers.
    xorg.enable = mkEnableOption "xorg";
    wayland.enable = mkEnableOption "wayland";

    terminal = {
      emulator = mkOption {
        type = types.enum [ "kitty" ];
        description = "Default terminal";
        default = "kitty";
      };
      opacity = mkOption {
        type = types.str;
        description = "Terminals opacity. 1 is opaque, 0 is fully transparent";
        default = "0.9";
      };
    };
    mouseSpeed = mkOption {
      type = types.str;
      description = "Mouse speed, string representing a float between 0 and 1";
      example = "0.9";
    };
    work.enable = mkEnableOption "work modules";

    # Theming
    fontName = mkOption {
      type = types.str;
      description = "Font family name. Nerd fonts should be installed in modules/nixos/default.nix";
      example = "Cousine Nerd Font";
      default = "Cousine Nerd Font";
    };
    gtkTheme = {
      name = mkOption {
        type = types.str;
        description = "To find theme names, inspect the theme package content (fd -d 1 package-name /nix/store)";
        example = "Tokyonight-Dark; Matcha-aliz; Papirus-Dark";
      };
      package = mkOption {
        type = types.package;
        description = "To find packages: https://search.nixos.org/packages?channel=24.11&from=0&size=50&sort=relevance&type=packages&query=-gtk-theme";
        example = pkgs.tokyonight-gtk-theme;
      };
    };

    # Browser
    firefox = {
      defaultBrowser = mkOption {
        type = types.bool;
        default = true;
      };
      searchEngine = mkOption {
        type = types.enum [
          "DuckDuckGo"
          "Kagi"
          "Google"
        ];
        description = "Search engine to use on firefox";
        default = "Kagi";
      };
      hexDocElixirVersion = mkOption {
        type = types.str;
        description = "Elixir version for the Hex documentation search engine";
        default = "1.17.3";
      };
    };
  };
}
