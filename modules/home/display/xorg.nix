{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.xorg.enable {
    home.packages = with pkgs; [
      xdotool
      xcape
      xorg.xev
      xorg.xprop
    ];

    # X keyboard options
    home.keyboard = {
      options = [ "caps:escape" ];
    };

    xsession = {
      enable = true;
      numlock.enable = true;

      profileExtra = ''
        # Increase key speed via a rate change
        xset r rate 300 25
        # Turn off the caps lock since there is no longer a key for it.
        xset -q | grep "Caps Lock:\s*on" && xdotool key Caps_Lock

        ${cfg.vars.scripts.setWallpaper} &
        update-lock-image &
      '';
    };
  };
}
