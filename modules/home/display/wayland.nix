{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf cfg.wayland.enable {
  home.packages = with pkgs; [ wev ];
}
