{ config, pkgs, ... }:
let
  cfg = config.thlr;
in
{
  imports = [
    ./xorg.nix
    ./hyprland
    ./qtile
    ./wayland.nix
  ];

  # To find packages:
  config = {
    gtk = with cfg.gtkTheme; {
      enable = true;
      #font.name = "DejaVu Sans 11";
      font.name = cfg.fontName + " 11";
      theme = {
        inherit name package;
      };
      iconTheme = {
        name = "Papirus-Dark";
        package = pkgs.papirus-icon-theme;
      };
    };
  };
}
