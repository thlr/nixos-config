# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# from libqtile.utils import guess_terminal

from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import (Click, Drag, DropDown, Group, Key, Match,
                             ScratchPad, Screen)
from libqtile.lazy import lazy
#from libqtile.log_utils import logger

# Each machine defines its own things. For now it is named autostart but the name could be better.
from autostart import *
import os

MOD = "mod4"
# terminal = guess_terminal()
TERMINAL = "kitty"
FONT = "JetBrainsMono"
HOME = os.environ["HOME"]

MUTE_CMD = f"{HOME}/.local/bin/volume_mute"
INCR_VOL_CMD = f"{HOME}/.local/bin/volume_incr"
DECR_VOL_CMD = f"{HOME}/.local/bin/volume_decr"

INCR_BRIGHTNESS = f"{HOME}/.local/bin/brightness_incr"
DECR_BRIGHTNESS = f"{HOME}/.local/bin/brightness_decr"

# TOGGLE_TOUCHPAD = '''
# sh -c "(synclient | grep 'TouchpadOff.*2' && synclient TouchpadOff=0) || synclient TouchpadOff=1"
#'''

keys = [
    # Layouts
    Key([MOD], "a", lazy.next_layout()),
    Key([MOD], "f", lazy.layout.maximize()),
    Key([MOD], "equal", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Switch focus
    Key(
        [MOD, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"
    ),
    Key(
        [MOD, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right",
    ),
    Key([MOD, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([MOD, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Move windows
    Key([MOD], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([MOD], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([MOD], "j", lazy.layout.down(), desc="Move focus down"),
    Key([MOD], "k", lazy.layout.up(), desc="Move focus up"),
    # Grow windows
    Key([MOD, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key(
        [MOD, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"
    ),
    Key([MOD, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([MOD, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    # Spawn and manage applications
    Key(
        [MOD, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([MOD], "Return", lazy.spawn(TERMINAL), desc="Launch terminal"),
    Key([MOD], "q", lazy.window.kill(), desc="Kill focused window"),
    # Key([MOD], "d", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([MOD], "d", lazy.spawn("rofi -show drun"), desc="Spawn a command using a prompt widget"),
    Key([MOD], "Space", lazy.window.toggle_floating(), desc="Toggle floating window"),
    # Session control
    Key([MOD], "Backspace", lazy.spawn("session_lock"), desc="Lock the screen"),
    Key([MOD, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([MOD, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Toggle between two groups
    Key([MOD], "Tab", lazy.screen.toggle_group(), desc="Toggle between two groups"),
    Key([], "Print", lazy.spawn("flameshot gui")),
    # Hardware bindings
    Key([], "XF86AudioMute", lazy.spawn(MUTE_CMD)),
    Key([MOD], "m", lazy.spawn(MUTE_CMD)),
    Key([], "XF86AudioRaiseVolume", lazy.spawn(INCR_VOL_CMD)),
    Key([], "XF86AudioLowerVolume", lazy.spawn(DECR_VOL_CMD)),
    Key([MOD], "asterisk", lazy.spawn(INCR_VOL_CMD)),
    Key([MOD], "ugrave", lazy.spawn(DECR_VOL_CMD)),
    Key([], "XF86MonBrightnessUp", lazy.spawn(INCR_BRIGHTNESS)),
    Key([], "XF86MonBrightnessDown", lazy.spawn(DECR_BRIGHTNESS)),
    Key([MOD], "comma", lazy.next_screen(), desc="Change focus to next screen"),
    #    Key([MOD], "n", lazy.spawn(TOGGLE_TOUCHPAD)),
]

group_names = [
    "ampersand",
    "Eacute",
    "quotedbl",
    "apostrophe",
    "parenleft",
    "minus",
    "Egrave",
    "underscore",
    "ccedilla",
]
group_labels = "123456789"
# autostart is imported from autostart.py
groups = []
for name, label in zip(group_names, group_labels):
    if label in autostart:
        groups.append(Group(name=name, label=label, spawn=autostart[label]))
    else:
        groups.append(Group(name=name, label=label))

# Scratchpad
groups.extend(
    [
        ScratchPad(
            name="0",
            dropdowns=[
                DropDown(
                    name="scratchpad",
                    cmd=TERMINAL,
                    height=0.8,
                    width=0.8,
                    x=0.1,
                    y=0.1,
                    opacity=1,
                )
            ],
        )
    ]
)
keys.extend([Key([MOD], "z", lazy.group["0"].dropdown_toggle("scratchpad"))])

for group in groups:
    print(f"Adding group {group}")
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [MOD],
                group.name,
                lazy.group[group.name].toscreen(),
                desc="Switch to group {}".format(group.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [MOD, "shift"],
                group.name,
                lazy.window.togroup(group.name, switch_group=False),
                desc="Switch to & move focused window to group {}".format(group.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

# https://coolors.co/03080b-0b2c33-124141-457867-917f41-684c17-583614-512713-4e1d19-471e1f
USE_COLOR_PALETTE = False
COLOR_PALETTE = [
    "#03080B",
    "#0B2C33",
    "#124141",
    "#457867",
    "#917F41",
    "#684C17",
    "#583614",
    "#512713",
    "#4E1D19",
    "#471E1F"
]

COLOR_FOCUS_1 = "#881111"
COLOR_FOCUS_2 = "#d75f5f"
COLOR_NORMAL = "#220000"
BORDER_WIDTH = 3
BAR_FOCUSED_OPACITY = 1
BAR_UNFOCUSED_OPACITY = 0.8
BAR_UNFOCUSED_COLOR = "#000000"
BAR_FOCUSED_COLOR = "#191919"
layouts = [
    layout.Columns(
        border_width=BORDER_WIDTH,
        border_normal=COLOR_NORMAL,
        border_focus=COLOR_FOCUS_1,
        border_focus_stack=COLOR_FOCUS_2,
    ),
    layout.Max(),
    layout.VerticalTile(
        border_width=BORDER_WIDTH,
        border_normal=COLOR_NORMAL,
        border_focus=COLOR_FOCUS_1,
    ),
]

widget_defaults = dict(
    font=FONT,
    fontsize=13,
)
left_widgets = [
    widget.CurrentLayout(**widget_defaults),
    widget.GroupBox(**widget_defaults),
    widget.Prompt(**widget_defaults),
    widget.WindowName(**widget_defaults),
    widget.Chord(
        **widget_defaults,
        chords_colors={
            "launch": ("#ff0000", "#ffffff"),
        },
        name_transform=lambda name: name.upper(),
    ),
]
clock_widget = widget.Clock(
    **widget_defaults,
    format="%a %d %b %Y %I:%M",
    background=COLOR_PALETTE[5] if USE_COLOR_PALETTE else None,
)

# Bars
screen_bars = [
    # Screen 1
    bar.Bar(
        [
            *left_widgets,
            # Right most widgets
            # widget.Net(**widget_defaults),
            widget.Volume(
                **widget_defaults,
                fmt="vol {}",
                emoji=False,
                background=COLOR_PALETTE[0] if USE_COLOR_PALETTE else None,
            ),
            widget.BatteryIcon(
                **widget_defaults,
                background=COLOR_PALETTE[1] if USE_COLOR_PALETTE else None,
            ),
            widget.Battery(
                **widget_defaults,
                notify_below=20,
                update_interval=5,
                format="{percent:2.0%} {hour:d}:{min:02d}",
                background=COLOR_PALETTE[2] if USE_COLOR_PALETTE else None,
            ),
            widget.Memory(
                **widget_defaults,
                measure_mem="G",
                background=COLOR_PALETTE[3] if USE_COLOR_PALETTE else None,
            ),
            widget.CPUGraph(
                **widget_defaults,
                background=COLOR_PALETTE[4] if USE_COLOR_PALETTE else None,
            ),
            widget.DF(
                **widget_defaults,
                visible_on_warn=True,
                warn_space=50,
                background=COLOR_PALETTE[5] if USE_COLOR_PALETTE else None,
            ),
            clock_widget,
            widget.Systray(
                **widget_defaults,
                background=COLOR_PALETTE[6] if USE_COLOR_PALETTE else None,
            ),
            widget.QuickExit(
                **widget_defaults,
                background=COLOR_PALETTE[7] if USE_COLOR_PALETTE else None,
            ),
        ],
        24,
        background=BAR_FOCUSED_COLOR
    ),
    # Screen 2
    bar.Bar(
        [*left_widgets, clock_widget],
        24,
        background=BAR_UNFOCUSED_COLOR
    ),
]
screens = [
    Screen(
        top=screen_bars[0],
    ),
    Screen(
        top=screen_bars[1],
    ),
]


# @hook.subscribe.current_screen_change
# def change_bar_color():
#     """
#     Change bar color depending on which screen is focused
#     """
#     for screen in qtile.screens:
#         if screen is qtile.current_screen:
#             screen.top.background = BAR_FOCUSED_COLOR
#         else:
#             screen.top.background = BAR_UNFOCUSED_COLOR
#         screen.top.draw()


# Drag floating layouts.
mouse = [
    Drag(
        [MOD],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [MOD], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([MOD], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(title="pinentry"),  # GPG key password entry
        Match(wm_class="Pavucontrol"),
        Match(wm_class="Blueman-manager"),
        Match(wm_class="Transmission-gtk"),
        Match(wm_class="zoom"),
        Match(wm_class="MEGAsync"),
        Match(title="Bluetooth Devices"),
        Match(wm_class="gnome-calculator"),
        Match(wm_class="org.gnome.Nautilus"),
    ]
)
auto_fullscreen = True
focus_on_window_activation = "never"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
