{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.qtile.enable {
    thlr.xorg.enable = true;
    home.packages = [
      pkgs.alsa-utils # amixer utility for the volume widget
    ];
    xdg.configFile."qtile/config.py".source = ./config.py;
  };
}
