{
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf cfg.hyprland.enable {
  wayland.windowManager.hyprland.settings.bindl = [
    ", switch:Lid Switch, exec, ${cfg.vars.scripts.sessionLock}"
    #'', switch:on:Lid Switch, exec, hyprctl keyword monitor "${cfg.hyprland.internalMonitorIdentifier}, disable"''
    #'', switch:off:Lid Switch, exec, hyprctl keyword monitor "${cfg.hyprland.internalMonitorIdentifier}, preferred, 0x0, 1"''
  ];
}
