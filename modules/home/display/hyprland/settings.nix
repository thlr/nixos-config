{
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf cfg.hyprland.enable {

  wayland.windowManager.hyprland.settings = {
    #debug.disable_logs = false;

    monitor = [
      "${cfg.hyprland.internalMonitorIdentifier},preferred,0x0,1"
      # For plugging in random monitors
      ",preferred,auto,1"
    ];

    "$mod" = "SUPER";

    exec-once = [
      "waybar"
    ] ++ cfg.hyprland.extraStartup;

    exec = [
      "${cfg.vars.scripts.setWallpaper}"
    ];

    misc = {
      disable_hyprland_logo = true;
      enable_swallow = true;
      # this regex takes window *classes*
      swallow_regex = "^(${cfg.terminal.emulator})$";
      # this regex takes window *titles*
      swallow_exception_regex = "^(wev)$";
    };

    general = {
      gaps_out = 10;
      layout = "master";
    };

    input = {
      kb_layout = "fr";
      kb_options = "caps:escape";
      repeat_rate = 25;
      repeat_delay = 300;
      touchpad.natural_scroll = true;
    };

    binds = {
      allow_workspace_cycles = true;
    };

    master = {
      mfact = 0.6;
    };

    group = {
      groupbar = {
        enabled = true;
        height = 8;
        render_titles = false;
        # Set this option when hyprland 47.2 is available
        #indicator_height = 0;
      };
    };
  };
}
