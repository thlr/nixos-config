#!/usr/bin/env bash

if [[ $(hyprctl -j getoption general:layout | jq -r '.str') = "master" ]]; then
  hyprctl keyword general:layout "dwindle"
else
  hyprctl keyword general:layout "master"
fi

# Notify the waybar layout module
pkill -RTMIN+1 waybar
