{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  imports = [
    ./bindings.nix
    ./rules.nix
    ./decorations.nix
    ./settings.nix
    ./switches.nix
  ];

  config = mkIf cfg.hyprland.enable {
    thlr.wayland.enable = true;
    home.packages = [
      pkgs.jq # For the toggle layout script (TODO: direct reference subsitued with subsituteAll)
      pkgs.wl-clipboard # For copying things from and to neovim
    ];
    # https://wiki.hyprland.org/Nix/Hyprland-on-Home-Manager/
    wayland.windowManager.hyprland = {
      enable = true;
      # TODO update to null?
      #package = pkgs.unstable.hyprland;
      # Enables hyprland-session.target which links to graphical-session.target
      systemd.enable = true;

      # Compatility layer between wayland and xorg apps
      xwayland.enable = true;
    };
    xdg.portal = {
      enable = true;
      extraPortals = [ pkgs.xdg-desktop-portal-hyprland ];
      config.common.default = [ "hyprland" ];
    };
  };
}
