{
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
# Window Rules V2: https://wiki.hyprland.org/Configuring/Window-Rules/#window-rules-v2
# Workspace Rules: https://wiki.hyprland.org/Configuring/Workspace-Rules/
mkIf cfg.hyprland.enable {
  wayland.windowManager.hyprland.settings = {
    windowrulev2 = [
      # Floating windows.
      # Identify windows with `hyprctl clients`
      "float, class:com.saivert.pwvucontrol"
      "float, class:gnome-calculator"
      "float, class:org.gnome.Nautilus"
      "float, class:MEGAsync"
      "float, class:org.gnome.Calculator"
      "float, title:^(.*)(Bitwarden\sPassword\sManager)(.*)$"
      # Pin windows to workspaces
      "workspace 1, class:firefox"
      "workspace 7, class:spotify"
      "workspace 7, class:keepassxc"
      "workspace 8, class:obsidian"
      "workspace 9, class:signal"
      "workspace 9, class:discord"
      # Smart gaps
      "bordersize 0, floating:0, onworkspace:w[tv1] s[false]"
      "rounding 0, floating:0, onworkspace:w[tv1] s[false]"
      "bordersize 0, floating:0, onworkspace:f[1] s[false]"
      "rounding 0, floating:0, onworkspace:f[1] s[false]"
    ];
    workspace = [
      # Disable rounding except for special workspaces
      "s[false], rounding: false"
      # Smart gaps (disabled for special workspaces)
      "w[tv1] s[false], gapsout:0, gapsin:0"
      "f[1] s[false], gapsout:0, gapsin:0"
      # Scratchpad
      "special:scratchpad, on-created-empty: [workspace special:scratchpad] kitty"
    ];
  };
}
