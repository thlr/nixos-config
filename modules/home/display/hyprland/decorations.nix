{
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf cfg.hyprland.enable {
  wayland.windowManager.hyprland.settings = {
    # Disable for now, but I hope to customize them later on
    animations.enabled = false;
    decoration = {
      rounding = 6;
      blur = {
        enabled = true;
        size = 3;
        passes = 2;
      };
    };
  };
}
