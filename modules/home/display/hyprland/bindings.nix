{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.hyprland.enable {
    wayland.windowManager.hyprland = {
      # Using this option instead of the standard settings options allows me to keep bindings in the order I want which is necessary for submaps.
      extraConfig = concatStringsSep "\n" [
        # Close window
        "bind = $mod, Q, killactive,"
        # Workspaces
        "bind = $mod, ampersand, workspace, 1"
        "bind = $mod SHIFT, ampersand, movetoworkspace, 1"
        "bind = $mod, Eacute, workspace, 2"
        "bind = $mod SHIFT, Eacute, movetoworkspace, 2"
        "bind = $mod, quotedbl, workspace, 3"
        "bind = $mod SHIFT, quotedbl, movetoworkspace, 3"
        "bind = $mod, apostrophe, workspace, 4"
        "bind = $mod SHIFT, apostrophe, movetoworkspace, 4"
        "bind = $mod, parenleft, workspace, 5"
        "bind = $mod SHIFT, parenleft, movetoworkspace, 5"
        "bind = $mod, minus, workspace, 6"
        "bind = $mod SHIFT, minus, movetoworkspace, 6"
        "bind = $mod, egrave, workspace, 7"
        "bind = $mod SHIFT, egrave, movetoworkspace, 7"
        "bind = $mod, underscore, workspace, 8"
        "bind = $mod SHIFT, underscore, movetoworkspace, 8"
        "bind = $mod, ccedilla, workspace, 9"
        "bind = $mod SHIFT, ccedilla, movetoworkspace, 9"
        "bind = $mod, Z, togglespecialworkspace, scratchpad"
        "bind = $mod, Z, moveworkspacetomonitor, special:scratchpad current"
        "bind = $mod SHIFT, agrave, movetoworkspace, special:scratchpad"
        "bind = $mod, Tab, workspace, previous"
        # Focus windows
        "bind = $mod, H, movefocus, l"
        "bind = $mod, J, movefocus, d"
        "bind = $mod, K, movefocus, u"
        "bind = $mod, L, movefocus, r"
        # Move windows
        "bind = $mod SHIFT, H, movewindoworgroup, l"
        "bind = $mod SHIFT, J, movewindoworgroup, d"
        "bind = $mod SHIFT, K, movewindoworgroup, u"
        "bind = $mod SHIFT, L, movewindoworgroup, r"
        # Master layout binds
        "bind = $mod, N, layoutmsg, focusmaster"
        "bind = $mod SHIFT, N, layoutmsg, swapwithmaster"
        "bind = $mod, B, submap, master_orientation"
        "submap = master_orientation"
        "bind = , H, layoutmsg, orientationleft"
        "bind = , J, layoutmsg, orientationbottom"
        "bind = , K, layoutmsg, orientationtop"
        "bind = , L, layoutmsg, orientationright"
        "bind = , escape, submap, reset"
        "submap = reset"
        # Change layout
        "bind = $mod, F, fullscreen, 0"
        "bind = $mod, U, exec, ${./toggle_layout.sh}"
        # Groups
        "bind = $mod, A, togglegroup"
        "bind = $mod SHIFT, A, lockactivegroup, toggle"
        "bind = $mod, E, changegroupactive, f"
        # Floating windows control
        "bind = $mod, Space, togglefloating, active"
        "bindm = $mod, mouse:272, movewindow"
        "bindm = $mod, mouse:273, resizewindow"
        # Move and resize floating windows
        "bind = $mod, W, submap, resize"
        "submap = resize"
        "binde = , H, resizeactive, -10 0"
        "binde = , J, resizeactive, 0 10"
        "binde = , K, resizeactive, 0 -10"
        "binde = , L, resizeactive, 10 0"
        "binde = SHIFT, H, moveactive, -10 0"
        "binde = SHIFT, J, moveactive, 0 10"
        "binde = SHIFT, K, moveactive, 0 -10"
        "binde = SHIFT, L, moveactive, 10 0"
        "bind = , escape, submap, reset"
        "submap = reset"
        # Monitors
        "bind = $mod, comma, focusmonitor, +1"
        "bind = $mod SHIFT, comma, movecurrentworkspacetomonitor, +1"
        # Lock screen and logout
        "bind = $mod, BackSpace, exec, ${cfg.vars.scripts.sessionLock}"
        "bind = $mod SHIFT, BackSpace, exec, ${pkgs.hyprland}/bin/hyprctl dispatch exit"
        # Open terminal
        "bind = $mod, Return, exec, ${cfg.terminal.emulator}"
        # Screenshot
        "bind = , Print, exec, ${cfg.vars.scripts.screenshot}" # Screenshot
        # Notifications
        "bind = Control_L, Space, exec, ${pkgs.dunst}/bin/dunstctl close-all"
        # Open launchpad
        "bind = $mod, D, exec, fuzzel"
        # Volume control
        "bindl = , XF86AudioMute, exec, ${cfg.vars.scripts.volumeMute}"
        "bindl = $mod, m, exec, ${cfg.vars.scripts.volumeMute}"
        "bindel = , XF86AudioRaiseVolume, exec, ${cfg.vars.scripts.volumeIncrease}"
        "bindel = , XF86AudioLowerVolume, exec, ${cfg.vars.scripts.volumeDecrease}"
        "bindel = $mod, Up, exec, ${cfg.vars.scripts.volumeIncrease}"
        "bindel = $mod, Down, exec, ${cfg.vars.scripts.volumeDecrease}"
        "bindel = $mod, asterisk, exec, ${cfg.vars.scripts.volumeIncrease}"
        "bindel = $mod, ugrave, exec, ${cfg.vars.scripts.volumeDecrease}"
        # Microphone control
        "bindl = , XF86AudioMicMute, exec, ${cfg.vars.scripts.micMute}"
        "bindl = $mod SHIFT, m, exec, ${cfg.vars.scripts.micMute}"
        # Media control
        "bindl = , XF86AudioPlay, exec, ${cfg.vars.scripts.mediaPlayPause}"
        "bindl = , XF86AudioNext, exec, ${cfg.vars.scripts.mediaNext}"
        "bindl = , XF86AudioPrev, exec, ${cfg.vars.scripts.mediaPrev}"
        # Brightness control
        "bindel = , XF86MonBrightnessUp, exec, ${cfg.vars.scripts.brightnessIncrease}"
        "bindel = , XF86MonBrightnessDown, exec, ${cfg.vars.scripts.brightnessDecrease}"
        "bindel = $mod, Right, exec, ${cfg.vars.scripts.brightnessIncrease}"
        "bindel = $mod, Left, exec, ${cfg.vars.scripts.brightnessDecrease}"
        "bindel = $mod, dollar, exec, ${cfg.vars.scripts.brightnessIncrease}"
        "bindel = $mod, dead_circumflex, exec, ${cfg.vars.scripts.brightnessDecrease}"
        # Panic bindings
        "bindl = $mod, R, exec, ${pkgs.hyprland}/bin/hyprctl reload"
        "bindl = $mod SHIFT, BackSpace, exec, ${pkgs.hyprland}/bin/hyprctl dispatch exit"
      ];
    };
  };
}
