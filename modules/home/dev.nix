{ pkgs, config, ... }:
let
  cfg = config.thlr;
in
{
  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
    enableZshIntegration = true;
    # man direnv.toml for the full list of options
    config = {
      hide_env_diff = true;
    };
  };

  # Replaces tabs in yaml files with spaces
  home.file."${cfg.vars.binDirPath}/yamltabs" = {
    executable = true;
    text = ''
      #! /usr/bin/env bash
      find . -type f -regex '.*\.ya?ml' -exec sed 's/\t/  /g' -i {} \;
    '';
  };

  home = {
    shellAliases = {
      g = "git";
      j = "just --unstable";
      lg = "lazygit";
    };
    packages = with pkgs; [
      just
      poetry
    ];
  };

  programs.git = {
    enable = true;
    userName = "Théo Larue";
    aliases = {
      co = "checkout";
      ci = "commit";
      st = "status";
      ps = "push";
      p = "pull";
    };
    extraConfig = {
      pull.rebase = true;
    };
  };

  programs.lazygit = {
    enable = true;
    settings = {
      os.edit = "nvim";
      gui.nerdFontsVersion = 3;
      notARepository = "quit";
    };
  };
}
