{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.work.enable {
    home.file = {
      ".config/kubectl_aliases".source = ./kubectl_aliases;
      "${cfg.vars.binDirPath}/kubectl-secret-decode" = {
        executable = true;
        text = ''
          #! /usr/bin/env zsh
          kubectl get secret $1 1> /dev/null || exit 1
          kubectl get secret $1 -o json | jq '.data | map_values(@base64d)'
        '';
      };

      "${cfg.vars.binDirPath}/kubectl-watch" = {
        executable = true;
        text = ''
          #! /usr/bin/env zsh
          watch kubectl get pods -o wide
        '';
      };

      "${cfg.vars.binDirPath}/kubectl-secret-usage" = {
        executable = true;
        text = ''
          #! /usr/bin/env zsh
          kubectl get secret $1 1> /dev/null || exit 1
          kubectl get pods -o json -A | jq ".items[] | select(.spec.containers[].env[]?.valueFrom.secretKeyRef.name==\"$1\") | {name: .metadata.name, namespace: .metadata.namespace}" | jq --slurp 'unique_by(.name)'
        '';
      };
    };

    home.packages = with pkgs; [
      kubectl
      jq # kubectl decode plugin dependency
      krew
      kubernetes-helm
      kubernetes-helmPlugins.helm-diff
    ];
  };
}
