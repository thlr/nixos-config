{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  imports = [ ./kube.nix ];

  config = mkIf cfg.work.enable {
    home.packages = with pkgs; [
      # gui
      keepassxc
      unstable.dbeaver-bin
      kleopatra
      drawio
      bruno

      # tui
      terraform
      docker-compose
      postgresql
      gnumake
      dyff
      backblaze-b2
    ];

    home.shellAliases.b2 = "backblaze-b2";
  };
}
