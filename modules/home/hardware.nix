{
  pkgs,
  config,
  lib,
  ...
}:
let
  cfg = config.thlr;
in
with lib;
{
  config = {
    services = {
      # https://nixos.wiki/wiki/Bluetooth#Using_Bluetooth_headset_buttons_to_control_media_player
      mpris-proxy.enable = true;

      # Equalizer and other effects for pipewire
      easyeffects.enable = true;
    };

    home.packages = with pkgs; [
      mons # quickly change monitor config
      brightnessctl
      playerctl
    ];
    # For backwards compatibility with qtile config, scripts are written in executable files
    thlr.vars.scripts = {
      brightnessIncrease = "${cfg.vars.binDirFullPath}/brightness_incr";
      brightnessDecrease = "${cfg.vars.binDirFullPath}/brightness_decr";
      volumeIncrease = "${cfg.vars.binDirFullPath}/volume_incr";
      volumeDecrease = "${cfg.vars.binDirFullPath}/volume_decr";
      volumeMute = "${cfg.vars.binDirFullPath}/volume_mute";
      micMute = "wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle";
      mediaPlayPause = "playerctl play-pause";
      mediaNext = "playerctl next";
      mediaPrev = "playerctl previous";
    };
    home.file = {
      "${cfg.vars.binDirPath}/brightness_incr" = {
        executable = true;
        text = ''
          #!/usr/bin/env bash
          brightnessctl set 5%+
        '';
      };
      "${cfg.vars.binDirPath}/brightness_decr" = {
        executable = true;
        text = ''
          #!/usr/bin/env bash
          brightnessctl set 5%-
        '';
      };
      "${cfg.vars.binDirPath}/volume_incr" = {
        executable = true;
        text = ''
          #!/usr/bin/env bash
          wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
        '';
      };
      "${cfg.vars.binDirPath}/volume_decr" = {
        executable = true;
        text = ''
          #!/usr/bin/env bash
          wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
        '';
      };
      "${cfg.vars.binDirPath}/volume_mute" = {
        executable = true;
        text = ''
          #!/usr/bin/env bash
          wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
        '';
      };
    };
  };
}
