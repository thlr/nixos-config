{ lib, config, ... }:
with lib;
let
  cfg = config.thlr;
in
{
  imports = [
    ../options.nix
    ./variables.nix
    ./display
    ./gui
    ./tui
    ./work
    ./dev.nix
    ./hardware.nix
  ];
  config = {
    # Let home manager install and manage itself
    programs.home-manager.enable = true;

    home = {
      stateVersion = "23.05";
      # Extra directories to add to PATH
      sessionPath = [
        "${cfg.vars.binDirFullPath}"
      ] ++ optionals cfg.work.enable [ "${config.home.homeDirectory}/.krew/bin" ];
    };

    nix.gc = {
      automatic = true;
      options = "--delete-older-than 30d";
    };
  };
}
