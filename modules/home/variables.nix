{ lib, config, ... }:
with lib;
let
  cfg = config.thlr;
in
{
  options.thlr.vars = {
    binDirPath = mkOption {
      type = types.str;
      default = ".local/bin";
    };
    binDirFullPath = mkOption {
      type = types.str;
      default = "${config.home.homeDirectory}/${cfg.vars.binDirPath}";
    };
    scripts = {
      brightnessIncrease = mkOption {
        type = types.str;
      };
      brightnessDecrease = mkOption {
        type = types.str;
      };
      volumeIncrease = mkOption {
        type = types.str;
      };
      volumeDecrease = mkOption {
        type = types.str;
      };
      volumeMute = mkOption {
        type = types.str;
      };
      micMute = mkOption {
        type = types.str;
      };
      mediaPlayPause = mkOption {
        type = types.str;
      };
      mediaNext = mkOption {
        type = types.str;
      };
      mediaPrev = mkOption {
        type = types.str;
      };
      screenshot = mkOption {
        type = types.str;
      };
      sessionLock = mkOption {
        type = types.str;
      };
      setWallpaper = mkOption {
        type = types.str;
      };
    };
  };
}
