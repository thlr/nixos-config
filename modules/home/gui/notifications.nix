{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  options.thlr.vars = {
    toggleNotifications = mkOption {
      type = types.str;
      default = "${cfg.vars.binDirFullPath}/toggle_notifications";
    };
    notificationTray = mkOption {
      type = types.str;
      default = "${cfg.vars.binDirFullPath}/notification_tray";
    };
  };
  config = {
    home.packages = with pkgs; [
      # Test the notification system
      notify-desktop
    ];

    home.file = {
      "${cfg.vars.binDirPath}/toggle_notifications" = {
        executable = true;
        text = ''
          #!/usr/bin/env zsh
          dunstctl set-paused toggle
        '';
      };
      "${cfg.vars.binDirPath}/notification_tray" = {
        executable = true;
        text = ''
          #!/usr/bin/env zsh
          COUNT=$(dunstctl count waiting)
          ENABLED=
          DISABLED=
          if [ $COUNT != 0 ]; then DISABLED=" $COUNT"; fi
          if dunstctl is-paused | grep -q "false" ; then echo $ENABLED; else echo $DISABLED; fi
        '';
      };
    };
    services.dunst = {
      enable = true;
      settings = {
        global = {
          font = "${cfg.fontName} 11";
          markup = "yes";
          plain_text = "no";
          format = "<b>%s</b>\n%b";
          sort = "no";
          indicate_hidden = "yes";
          alignment = "left";
          bounce_freq = "0";
          show_age_threshold = "-1";
          word_wrap = "yes";
          ignore_newline = "no";
          stack_duplicate = "yes";
          hide_duplicates_count = "yes";
          idle_threshold = "0";
          monitor = "0";
          sticky_history = "yes";
          icon_position = "left";
          max_icon_size = "50";
        };
        urgency_low = {
          timeout = 4;
        };
        urgency_normal = {
          timeout = 6;
        };
        urgency_critical = {
          timeout = 8;
        };
      };
    };
  };
}
