{
  lib,
  pkgs,
  config,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkMerge [
  (mkIf (cfg.utilities.screenshot == "grim") {
    home.packages = with pkgs; [
      grim
      slurp
      swappy
    ];
    thlr.vars.scripts.screenshot = ''export geo=$(slurp); sleep 2; grim -g "$geo" - | swappy -f -'';
  })

  (mkIf (cfg.utilities.screenshot == "flameshot") {
    thlr.vars.scripts.screenshot = "flameshot gui";
    services.flameshot = mkMerge [
      # Special build for wayland https://wiki.nixos.org/wiki/Flameshot
      # I am not sure how well this works for Hyprland, given that it is not based on wlroots anymore.
      # Also enableWlrSupport is only available on unstable.
      (mkIf cfg.wayland.enable {
        package = pkgs.unstable.flameshot.override { enableWlrSupport = true; };
      })
      {
        enable = true;
        # https://github.com/flameshot-org/flameshot/blob/master/flameshot.example.ini
        settings = {
          General = {
            showStartupLaunchMessage = false;
            showDesktopNotification = false;
            disabledTrayIcon = true;
            disabledGrimWarning = true;
          };
        };
      }
    ];
  })
]
