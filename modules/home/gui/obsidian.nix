{
  pkgs,
  ...
}:
{
  home.packages = [ pkgs.unstable.obsidian ];
}
