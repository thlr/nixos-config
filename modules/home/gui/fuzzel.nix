{ config, lib, ... }:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.wayland.enable {
    programs.fuzzel = {
      enable = true;
      settings = {
        main.prompt = "❯   ";
        colors = {
          background = "282a36fa";
          selection = "3d4474fa";
          border = "fffffffa";
        };
      };
    };
  };
}
