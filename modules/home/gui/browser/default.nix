{
  imports = [
    ./firefox.nix
    ./firefox-search-engines.nix
    ./firefox-settings.nix
  ];
}
