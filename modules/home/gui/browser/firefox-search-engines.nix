{
  config,
  pkgs,
  ...
}:
let
  cfg = config.thlr;
in
{
  # All my additional custom search engines
  programs.firefox.profiles.thlr.search.engines = {
    "HexDocs" = {
      iconUpdateUrl = "https://hexdocs.pm/favicon.ico";
      updateInterval = 24 * 60 * 60 * 1000; # every day
      definedAliases = [ "@h" ];
      urls = [
        {
          template = "https://hexdocs.pm/elixir/${cfg.firefox.hexDocElixirVersion}/search.html";
          params = [
            {
              name = "q";
              value = "{searchTerms}";
            }
          ];
        }
      ];
    };
    "Linguee" = {
      iconUpdateUrl = "https://www.linguee.fr/favicon.ico";
      updateInterval = 24 * 60 * 60 * 1000; # every day
      definedAliases = [ "@l" ];
      urls = [
        {
          template = "https://www.linguee.fr/francais-anglais/search";
          params = [
            {
              name = "source";
              value = "auto";
            }
            {
              name = "query";
              value = "{searchTerms}";
            }
          ];
        }
      ];
    };
    "Discogs" = {
      iconUpdateUrl = "https://www.discogs.com/favicon.ico";
      updateInterval = 24 * 60 * 60 * 1000; # every day
      definedAliases = [ "@d" ];
      urls = [
        {
          template = "https://discogs.com/search";
          params = [
            {
              name = "q";
              value = "{searchTerms}";
            }
            {
              name = "type";
              value = "all";
            }
            {
              name = "format_exact";
              value = "Vinyl";
            }
            {
              name = "genre_exact";
              value = "Electronic";
            }
          ];
        }
      ];
    };
    # I am adding the Kagi engine myself because using the custom extension doesn't work - the default engine reverts to google every time I reboot.
    "Kagi" = {
      urls = [
        {
          template = "https://kagi.com/search";
          params = [
            {
              name = "q";
              value = "{searchTerms}";
            }
          ];
        }
        {
          template = "https://kagi.com/api/autosuggest";
          params = [
            {
              name = "q";
              value = "{searchTerms}";
            }
          ];
          type = "application/x-suggestions+json";
        }
      ];
      definedAliases = [ "@kagi" ];
      iconUpdateUrl = "https://kagi.com/favicon.ico";
      updateInterval = 24 * 60 * 60 * 1000; # every day
    };
    "Nix Packages" = {
      urls = [
        {
          template = "https://search.nixos.org/packages";
          params = [
            {
              name = "query";
              value = "{searchTerms}";
            }
          ];
        }
      ];
      icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
      definedAliases = [ "@np" ];
    };
    "Nixos Options" = {
      urls = [
        {
          template = "https://search.nixos.org/options";
          params = [
            {
              name = "query";
              value = "{searchTerms}";
            }
          ];
        }
      ];
      icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
      definedAliases = [ "@no" ];
    };
    "Youtube" = {
      urls = [
        {
          template = "https://www.youtube.com/results";
          params = [
            {
              name = "search_query";
              value = "{searchTerms}";
            }
          ];
        }
      ];
      iconUpdateUrl = "https://www.youtube.com/favicon.ico";
      updateInterval = 24 * 60 * 60 * 1000; # every day
      definedAliases = [ "@y" ];
    };
    "Wikipedia".metaData.alias = "@w";
    "Bing".metadata.hidden = true;
    "Amazon.fr".metadata.hidden = true;
  };
}
