{
  lib,
  config,
  inputs,
  system,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  xdg.mimeApps.defaultApplications = mkIf cfg.firefox.defaultBrowser {
    "text/html" = "firefox.desktop";
    "x-scheme-handler/http" = "firefox.desktop";
    "x-scheme-handler/https" = "firefox.desktop";
    "x-scheme-handler/about" = "firefox.desktop";
    "x-scheme-handler/unknown" = "firefox.desktop";
  };
  home.sessionVariables.DEFAULT_BROWSER = mkIf cfg.firefox.defaultBrowser "${pkgs.unstable.firefox}/bin/firefox";
  programs.firefox = {
    package = pkgs.unstable.firefox;
    enable = true;

    profiles = {
      thlr = {
        id = 0;
        path = "thlr";
        isDefault = true;

        # nix flake show "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons"
        extensions = with inputs.firefox-addons.packages."${system}"; [
          bitwarden
          dearrow
          istilldontcareaboutcookies
          keepassxc-browser
          vimium
          web-search-navigator
          ublock-origin
          #kagi-search
        ];

        search = {
          default = cfg.firefox.searchEngine;
          force = true; # Will force the replacement of the search config file
        };

        # Hide the tab bar
        # userChrome = ''
        #   #TabsToolbar { visibility: collapse !important; }
        # '';
      };
    };
  };
}
