{
  config,
  ...
}:
{
  # about:config
  programs.firefox.profiles.thlr.settings = {
    # generic
    "browser.download.dir" = "${config.home.homeDirectory}/Downloads";

    # Required for obsidian web clipper's highlighting feature
    "extensions.openPopupWithoutUserGesture.enabled" = true;

    # Enable or disable fingerprinting by website. A fingerprint enables a website to track a user accross several devices, based on the location, language etc.
    # This can have some inconveniencies https://support.mozilla.org/en-US/questions/1253204
    "privacy.resistFingerprinting" = false;

    # ui
    # Needed to hide the tab bar
    "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
    "layout.css.devPixelsPerPx" = 1.1; # Make UI bigger

    # search bar
    "browser.urlbar.suggest.quicksuggest.sponsored" = false;
    "browser.urlbar.suggest.quicksuggest.nonsponsored" = false;

    # telemetry
    "browser.newtabpage.activity-stream.telemetry" = false;
    "browser.newtabpage.activity-stream.feeds.telemetry" = false;
    "browser.ping-centre.telemetry" = false;
    "toolkit.telemetry.reportingpolicy.firstRun" = false;
    "toolkit.telemetry.unified" = false;
    "toolkit.telemetry.archive.enabled" = false;
    "toolkit.telemetry.updatePing.enabled" = false;
    "toolkit.telemetry.shutdownPingSender.enabled" = false;
    "toolkit.telemetry.newProfilePing.enabled" = false;
    "toolkit.telemetry.bhrPing.enabled" = false;
    "toolkit.telemetry.firstShutdownPing.enabled" = false;
    "datareporting.healthreport.uploadEnabled" = false;
    "datareporting.policy.dataSubmissionEnabled" = false;
    "app.shield.optoutstudies.enable" = false;

    # media
    "media.ffmpeg.vaapi.enabled" = true;
    "media.rdd-ffmpeg.enabled" = true;
    "media.navigator.mediadataencoder_vpx_enabled" = true;

    # passwords
    "signon.rememberSignons" = false;
    "signon.autofillForms" = false;
    "signon.generation.enabled" = false;
    "signon.management.page.breach-alerts.enabled" = false;
  };
}
