{ lib, config, ... }:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.xorg.enable {
    programs.rofi = {
      enable = true;
      font = "Cousine Nerd Font 12";
      theme = "gruvbox-dark";
    };
  };
}
