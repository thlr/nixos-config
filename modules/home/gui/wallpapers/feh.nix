{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf (cfg.utilities.wallpaperEngine == "feh") {
  thlr.vars.scripts.setWallpaper = "${cfg.vars.binDirFullPath}/setwallpaper";
  home = {
    packages = with pkgs; [ feh ];
    file."${cfg.vars.binDirPath}/setwallpaper" = {
      executable = true;
      text = ''
        #!/usr/bin/env zsh
        ${pkgs.feh}/bin/feh --bg-max --randomize ${../../../../pics/photos} --image-bg "white"
      '';
    };
  };
}
