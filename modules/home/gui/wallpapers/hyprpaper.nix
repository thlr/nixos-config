{
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf (cfg.utilities.wallpaperEngine == "hyprpaper") {
  thlr.vars.scripts.setWallpaper = "${cfg.vars.binDirFullPath}/setwallpaper";
  services.hyprpaper = {
    enable = true;
    settings = {
      # TODO
    };
  };
}
