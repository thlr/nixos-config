{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf (cfg.utilities.wallpaperEngine == "swww") {
  home.packages = [ pkgs.swww ];
  thlr.vars.scripts.setWallpaper = "${cfg.vars.binDirFullPath}/setwallpaper";
  home.file."${cfg.vars.binDirPath}/setwallpaper" = {
    executable = true;
    text = ''
      #!/usr/bin/env zsh

      # Chooses a random wallpaper, with a seed based on the current date.
      WALLPAPER="$(find ${../../../../pics/photos} -type f | sort -R | tail -n1)"
      ${pkgs.swww}/bin/swww img $WALLPAPER --resize fit --fill-color ffffff
    '';
  };
}
