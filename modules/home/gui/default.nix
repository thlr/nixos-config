{
  pkgs,
  ...
}:
{
  imports = [
    ./browser
    ./desktop-bar
    ./screen-locker
    ./wallpapers
    ./notifications.nix
    ./fuzzel.nix
    ./obsidian.nix
    ./rofi.nix
    ./screenshot.nix
  ];

  config = {
    services = {
      network-manager-applet.enable = true;
      megasync = {
        enable = true;
        #package = pkgs.unstable.megasync;
      };
    };

    xdg.mimeApps = {
      enable = true;
      defaultApplications = {
        "image/jpeg" = "org.gnome.gThumb.desktop";
        "image/png" = "org.gnome.gThumb.desktop";
        "x-olympus-org" = "org.gnome.gThumb.desktop";
        "text/plain" = "org.gnome.TextEditor.desktop";
        "inode/directory" = "org.gnome.Nautilus.desktop";
      };
    };

    home = {
      pointerCursor = {
        name = "Numix-Cursor-Light";
        package = pkgs.numix-cursor-theme;
      };

      packages = with pkgs; [
        unstable.spotify
        unstable.signal-desktop
        unstable.discord
        #unstable.armcord
        nautilus
        gnome-calculator
        #system-config-printer
        gthumb
        gnome-text-editor
        vlc
        pwvucontrol
        libreoffice
        chromium
      ];
    };
  };
}
