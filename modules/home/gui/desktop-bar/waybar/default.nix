{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.wayland.enable {
    home.packages = [ pkgs.wlogout ];
    programs.waybar = {
      enable = true;
      settings = [
        {
          layer = "top";
          position = "top";
          height = 24;
          modules-left = [
            "custom/layout"
            "hyprland/workspaces"
          ];
          modules-center = [ "hyprland/window" ];
          modules-right = [
            "wireplumber"
            "cpu"
            "memory"
            "battery"
            "custom/notifications"
            "tray"
            "clock"
            "custom/power"
          ];

          # Modules config
          #"hyprland/window" = {
          #  rewrite = {
          #    "(.*) — Mozilla Firefox" = "🌎 $1";
          #    "(.*) - Obsidian (.*)" = "📔 $1";
          #  };
          #};
          wireplumber = {
            format = "{volume}% {icon}";
            format-bluetooth = "{volume}% {icon}";
            format-muted = "";
            format-icons = {
              headphones = "";
              default = [
                ""
                ""
              ];
            };
            on-click = "pwvucontrol";
          };
          cpu = {
            format = "CPU {usage}%";
          };
          memory = {
            format = "MEM {percentage}%";
          };
          battery = {
            format = "{capacity}% {icon}";
            format-icons = [
              ""
              ""
              ""
              ""
              ""
            ];
          };
          tray = {
            spacing = 10;
          };
          clock = {
            format-alt = "{:%Y-%m-%d}";
          };
          "custom/power" = {
            format = " ⏻ ";
            tooltip = false;
            on-click = "wlogout";
          };
          "custom/layout" = {
            format = "  [ {} ]";
            exec = "${pkgs.hyprland}/bin/hyprctl -j getoption general:layout | ${pkgs.jq}/bin/jq -r .str";
            signal = 1;
          };
          "custom/notifications" = {
            format = "  {}  ";
            exec = "${cfg.vars.notificationTray}";
            on-click = "${cfg.vars.toggleNotifications}";
            restart-interval = 1;
          };
        }
      ];
      style = import ./style.nix { inherit (cfg) fontName; };
    };
  };
}
