{
  imports = [
    ./hyprlock.nix
    ./xss-lock.nix
    ./idle-daemon.nix
  ];
}
