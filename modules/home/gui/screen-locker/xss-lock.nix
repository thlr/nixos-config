{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf (cfg.utilities.sessionLocker == "xss-lock") {
  home.packages = with pkgs; [
    xss-lock
    betterlockscreen
  ];
  thlr.vars.scripts.sessionLock = "${cfg.vars.binDirFullPath}/session_lock";
  home.file = {
    # Run update-lock-image each time the lock image changes
    "${cfg.vars.binDirPath}/update-lock-image" = {
      text = ''
        #! /usr/bin/env zsh
        ${pkgs.betterlockscreen} -u ${../../../../pics/nixos.png} --fx ""
      '';
      executable = true;
    };
    "${cfg.vars.binDirPath}/session_lock" = {
      text = ''
        #! /usr/bin/env zsh
        ${pkgs.betterlockscreen}/bin/betterlockscreen -l
      '';
      executable = true;
    };
  };
  xdg.configFile = {
    "betterlockscreen".text = ''
      lock_timeout=0
      fx_list=()
      locktext="
    '';
  };
  services.betterlockscreen = {
    enable = false;
    inactiveInterval = 60;
  };
}
