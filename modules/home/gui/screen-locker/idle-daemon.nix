{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf (cfg.utilities.idleDaemon == "hypridle") {
  services.hypridle = {
    enable = true;
    settings = {
      general = {
        lock_cmd = "${cfg.vars.scripts.sessionLock}";
        before_sleep_cmd = "loginctl lock-session";
        after_sleep_cmd = "hyprctl dispatch dpms on"; # to avoid having to press a key twice to turn on the display.
      };
      listener = [
        {
          timeout = 300; # 5min
          on-timeout = "${pkgs.brightnessctl}/bin/brightnessctl -s set 10";
          on-resume = "${pkgs.brightnessctl}/bin/brightnessctl -r";
        }
        {
          timeout = 300; # 5min
          on-timeout = "${pkgs.brightnessctl}/bin/brightnessctl -sd rgb:kbd_backlight set 0"; # turn off keyboard backlight
          on-resume = "${pkgs.brightnessctl}/bin/brightnessctl -sd rgb:kbd_backlight set 0 "; # restore keyboard backlight
        }
        {
          timeout = 600; # 10min
          on-timeout = "loginctl lock-session"; # will trigger the "lock-cmd defined above"
        }
        {
          timeout = 600; # 10min
          on-timeout = "hyprctl dispatch dpms off"; # screen off
          on-resume = "hyprctl dispatch dpms on"; # screen on
        }
        {
          timeout = 1800; # 30min
          on-timeout = "systemctl suspend";
        }
      ];
    };
  };
}
