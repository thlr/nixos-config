{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf (cfg.utilities.sessionLocker == "hyprlock") {
  thlr.vars.scripts.sessionLock = "pidof hyprlock || hyprlock"; # Avoid running two instances of hyprlock
  programs.hyprlock = {
    enable = true;
    settings = {
      background = {
        path = "screenshot";
        blur_passes = 3;
        blur_size = 4;
        brightness = 0.5;
        vibrancy = 0.15;
      };
      image = {
        #path = "${../../../../pics/nix-snowflake-colours.png}";
      };
      input-field = {
        monitor = "";
        size = "50, 50";
        hide_input = true;
        rounding = -1;
        outline_thickness = "10";
      };
      shadowable = {
        shadow_passes = 2;
        shadow_size = 10;
      };
      label = {
        monitor = "";
        text = ''
          cmd[update:1000] echo "$(date +'%d %B %Y %R:%S')"
        '';
        font_size = 30;
        font_family = cfg.fontName;
        position = "-200, -100"; # 0, 0 is bottom left. position is relative to the alignment
        halign = "right";
        valign = "top";
      };
    };
  };
}
