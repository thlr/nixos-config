{ pkgs, ... }:
{

  # I do it this way for read-only system issues that clash with the plugin
  # manager. This is not ideal. Maybe some other solution such as
  # home-manager's own solution (https://nixos.wiki/wiki/Neovim) or some
  # community driven projects (https://github.com/nix-community/nixvim) would
  # be a better fit.
  xdg.configFile."nvim/lua".source = ./nvim/lua;
  xdg.configFile."nvim/ftplugin".source = ./nvim/ftplugin;
  xdg.configFile."nvim/init.lua".source = ./nvim/init.lua;

  home = {
    shellAliases.v = "nvim";
  };

  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    defaultEditor = true;
    #package = pkgs.unstable.neovim-unwrapped;
    withNodeJs = true;
    withPython3 = true;

    extraPython3Packages = (
      pypkgs: with pypkgs; [
        pynvim
        python-lsp-server
      ]
    );

    extraPackages = with pkgs; [
      #unstable.nodePackages.neovim
      nodejs
      tree-sitter
      yarn # Needed to install Markdown-preview plugin
      # tabnine

      gcc # Needed to compile some treesitter parsers

      xclip # Share clipboard with system
      ctags # browse local code

      # Language servers (python language servers are located in python.nix)
      # and their companions (e.g. linters). These should also be declared in
      # nvim/lua/plugins/lazy/lsp.lua
      #
      # Nix
      nil
      nixfmt-rfc-style
      # Bash
      nodePackages.bash-language-server
      shellcheck
      # Lua
      sumneko-lua-language-server
      # Terraform
      terraform-ls
      # Elixir
      elixir_ls
      # Yaml
      yaml-language-server
    ];
  };
}
