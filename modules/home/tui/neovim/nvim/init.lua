-- Don't change the order in which these files are loaded. utils, bindings and settings should include only native vim configuration and not depend on plugins.
-- Plugins configurations should be found in their own setup files.
if vim.fn.exists("g:vscode") == 0 then
  require("utils")
  require("bindings")
  require("settings")
  require("plugins")
end
