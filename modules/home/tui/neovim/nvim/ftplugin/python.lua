vim.bo.expandtab = true
-- tabs
-- For a summary of what each mean : https://arisweedler.medium.com/tab-settings-in-vim-1ea0863c5990
vim.bo.shiftwidth = 4
vim.bo.tabstop = 4
vim.bo.softtabstop = 4

require("cmp").setup.buffer({
  sources = {
    { name = "luasnip" },
    { name = "nvim_lsp" },
    --{ name = 'buffer' },
  },
})
