vim.bo.expandtab = true
-- tabs
-- For a summary of what each mean : https://arisweedler.medium.com/tab-settings-in-vim-1ea0863c5990
vim.bo.shiftwidth = 2
vim.bo.tabstop = 2
vim.bo.softtabstop = 2
