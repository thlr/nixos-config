return {
  "hrsh7th/cmp-nvim-lsp", -- Nvim-cmp source for neovim builtin LSP client
  "hrsh7th/cmp-buffer", -- nvim-cmp source for buffer words
  "hrsh7th/cmp-path", -- nvim-cmp source for paths
  {
    "windwp/nvim-autopairs",
    event = "InsertEnter",
    opts = {},
  },
  "honza/vim-snippets", -- snippets files for various programming languages.
  {
    -- snippet engine
    "L3MON4D3/LuaSnip",
    dependencies = "honza/vim-snippets",
    build = "make install_jsregexp",
    -- Disable default tab and s-tab behavior in luasnip to implement it in vim-cmp
    keys = function()
      return {}
    end,
  },
  "saadparwaiz1/cmp_luasnip", -- luasnip completion source for nvim-cmp (link between nvim-comp and luasnip)
  {
    "hrsh7th/nvim-cmp", -- Completion engine
    opts = function()
      --The following are for completion on Tab and S-Tab
      --from https://github.com/LazyVim/starter/blob/888600e7ff4b0d8be0db18db932632830bfb7804/lua/plugins/example.lua#L218
      local has_words_before = function()
        unpack = unpack or table.unpack
        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
      end
      local luasnip = require("luasnip")
      local cmp = require("cmp")
      return {
        mapping = {
          ["<C-b>"] = cmp.mapping.scroll_docs(-4),
          ["<C-f>"] = cmp.mapping.scroll_docs(4),
          ["<C-Space>"] = cmp.mapping.complete(),
          ["<C-e>"] = cmp.mapping.abort(),
          ["<CR>"] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
          ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
              luasnip.expand_or_jump()
            elseif has_words_before() then
              cmp.complete()
            else
              fallback()
            end
          end, { "i", "s" }),
          ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
              luasnip.jump(-1)
            else
              fallback()
            end
          end, { "i", "s" }),
          ["<C-Tab>"] = cmp.mapping(function(fallback)
            if luasnip.expand_or_jumpable() then
              luasnip.expand_or_jump()
            else
              fallback()
            end
          end, { "i", "s" }),
        },
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end,
        },
        -- List of sources for completion
        -- The order **matters** as it decides the priority of suggestions
        sources = {
          { name = "nvim_lsp" },
          { name = "luasnip" },
          { name = "buffer" },
          { name = "path" },
        },
        formatting = {
          format = require("lspkind").cmp_format({
            with_text = true,
            menu = {
              buffer = "[buf]",
              nvim_lsp = "[LSP]",
              nvim_lua = "[vim api]",
              path = "[path]",
              luasnip = "[snippet]",
              tn = "[TabNine]",
            },
          }),
        },
        experimental = {
          native_menu = false,
        },
      }
    end,
  },
}
