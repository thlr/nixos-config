return {
  "onsails/lspkind-nvim", -- vscode-like pictograms to neovim built-in lsp
  {
    "neovim/nvim-lspconfig",
  },
  {
    "VonHeikemen/lsp-zero.nvim",
    branch = "v4.x",
    init = function()
      local lsp_zero = require("lsp-zero")
      local lspconfig = require("lspconfig")

      local lsp_attach = function(_, bufnr)
        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end
        lsp_zero.buffer_autoformat() -- format on save

        -- Keymaps
        map("n", "K", vim.lsp.buf.hover, { desc = "Show information" })
        map("n", "gd", vim.lsp.buf.definition, { desc = "Go to definition" })
        map("n", "gD", vim.lsp.buf.declaration, { desc = "Go to declaration" })
        map("n", "gi", vim.lsp.buf.implementation, { desc = "Go to implementation" })
        map("n", "go", vim.lsp.buf.type_definition, { desc = "Go to type definition" })
        map("n", "gr", require("telescope.builtin").lsp_references, { desc = "Search references" })
        map("n", "gs", vim.lsp.buf.signature_help, { desc = "Signature help" })
        map("n", "<F2>", vim.lsp.buf.rename, { desc = "Rename symbol" })
        map({ "n", "x" }, "<F3>", function()
          vim.lsp.buf.format({ async = true })
        end, { desc = "Format file" })
        map("n", "<F4>", vim.lsp.buf.code_action, { desc = "Do code action" })
        map("n", "<leader>e", vim.diagnostic.open_float, { desc = "Open diagnostic" })
        map("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to next diagnostic" })
        map("n", "]d", vim.diagnostic.goto_next, { desc = "Go to previous diagnostic" })
      end

      lsp_zero.extend_lspconfig({
        sign_text = false,
        lsp_attach = lsp_attach,
        capabilities = require("cmp_nvim_lsp").default_capabilities(),
      })

      -- YAML
      lspconfig.yamlls.setup({
        settings = {
          yaml = {
            schemas = {
              -- Gitlab CI schema
              ["https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json"] = "*.gitlab-ci.yml",
            },
          },
        },
      })

      -- Terraform
      lspconfig.terraformls.setup({})

      -- Elixir
      lspconfig.elixirls.setup({
        cmd = { "elixir-ls" },
      })

      -- bash
      -- Disable diagnostics for .env files. Looking for a better way to do this.
      vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
        pattern = "*.env",
        callback = function()
          vim.diagnostic.enable(false)
        end,
      })
      lspconfig.bashls.setup({})

      -- lua
      lspconfig.lua_ls.setup({
        settings = {
          Lua = {
            runtime = {
              -- Tell the language server which version of Lua you're using
              -- (most likely LuaJIT in the case of Neovim)
              version = "LuaJIT",
            },
            diagnostics = {
              -- Get the language server to recognize the `vim` global
              globals = { "vim" },
            },
            workspace = {
              -- Make the server aware of Neovim runtime files
              library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
              enable = false,
            },
          },
        },
      })

      -- nix
      lspconfig.nil_ls.setup({
        settings = {
          ["nil"] = {
            formatting = {
              command = { "nixfmt" },
            },
          },
        },
      })

      -- Python
      lspconfig.pylsp.setup({
        enable = true,
        cmd = { "pylsp" },
        -- cmd = { 'pylsp', '-vvv', '--log-file', 'pylsp.log' },
        settings = {
          pylsp = {
            configurationSources = { "flake8" },
            plugins = {
              flake8 = {
                enabled = true,
                maxLineLength = 100,
                ignore = {
                  "D100", -- Missing docstring in public module
                  "D101", -- Missing docstring in public class
                  "D105", -- Missing docstring in magic method
                  "D107", -- Missing docstring in __init__
                  "D200", -- One-line docstring should fit on one line with quotes
                  "D205", -- 1 blank line required between summary line and description
                  "D400", -- First line should end with a period
                  "D402", -- First line should not be the function's "signature"
                  "D401", -- First line should be in imperative mood
                  "W503", -- line break before binary operator
                },
              },
              pylint = {
                enabled = true,
                -- https://vald-phoenix.github.io/pylint-errors/
                executable = "pylint",
                args = {
                  "--disable="
                    .. "C0115," -- missing-class-docstring
                    .. "C0116," -- missing-function-docstring
                    .. "R0902," -- too-many-instance-attributes
                    .. "R0903," -- too-few-public-methods
                    .. "R0915", -- too-many-statements
                },
              },
              pylsp_mypy = {
                enabled = true,
                live_mode = false,
              },
              jedi_completion = {
                enabled = true,
                fuzzy = true,
              },
              jedi_hover = {
                enabled = true,
              },
              jedi_references = {
                enabled = true,
              },
              jedi_signature_help = {
                enabled = true,
              },
              jedi_symbols = {
                enabled = true,
                all_scopes = true,
              },
              --pycodestyle = {
              --    enabled = false
              --},
              --yapf = {
              --    enabled = false
              --},
              --pyflakes = {
              --    enabled = false
              --},
              --pydocstyle = {
              --    enabled = false
              --},
              --mccabe = {
              --    enabled = false
              --},
              --preload = {
              --    enabled = false
              --},
            },
          },
        },
      })
    end,
  },
}
