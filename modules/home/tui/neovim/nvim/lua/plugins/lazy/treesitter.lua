return {
  --"towolf/vim-helm",
  -- https://www.lazyvim.org/plugins/treesitter
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    --lazy = vim.fn.argc(-1) == 0, -- load treesitter early when opening a file from the cmdline
    --init = function(plugin)
    --  -- PERF: add nvim-treesitter queries to the rtp and it's custom query predicates early
    --  -- This is needed because a bunch of plugins no longer `require("nvim-treesitter")`, which
    --  -- no longer trigger the **nvim-treesitter** module to be loaded in time.
    --  -- Luckily, the only things that those plugins need are the custom queries, which we make available
    --  -- during startup.
    --  require("lazy.core.loader").add_to_rtp(plugin)
    --  require("nvim-treesitter.query_predicates")
    --end,
    -- According to the doc https://github.com/nvim-treesitter/nvim-treesitter/wiki/Installation#lazynvim
    config = function()
      require("nvim-treesitter.configs").setup({
        ensure_installed = {
          "awk",
          "bash",
          "c",
          "css",
          "csv",
          "diff",
          "dockerfile",
          "eex",
          "elixir",
          "erlang",
          "git_config",
          "git_rebase",
          "gitattributes",
          "gitcommit",
          "gitignore",
          "go",
          "gomod",
          "gosum",
          "gotmpl",
          "gpg",
          "graphql",
          "hcl",
          "helm",
          "hjson",
          "html",
          "http",
          "hyprlang",
          "ini",
          "javascript",
          "jq",
          "json",
          "json5",
          "jsonnet",
          "just",
          "lua",
          "luadoc",
          "luau",
          "make",
          "markdown",
          "markdown_inline",
          "nix",
          "promql",
          "python",
          "regex",
          "sql",
          "ssh_config",
          "terraform",
          "toml",
          "xml",
          "yaml",
          "zathurarc",
        },
        highlight = {
          enable = true,
        },
        indent = {
          -- Warning : treesitter indentation is still experimental
          enable = false,
          disable = { "yaml" },
        },
        incremental_selection = {
          enable = true,
          keymaps = {
            init_selection = "<C-n>",
            node_incremental = "<C-n>",
            node_decremental = "<C-r>",
            scope_incremental = false,
          },
        },
      })
    end,
  },
}
