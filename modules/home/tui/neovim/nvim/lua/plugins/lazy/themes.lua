return {
  -- "xiyaowong/transparent.nvim", -- transparent bg
  -- 'akinsho/horizon.nvim',
  "morhetz/gruvbox",
  {
    "joshdick/onedark.vim",
    --init = function()
    --  vim.cmd("colorscheme onedark")
    --end,
  },
  "jacoborus/tender.vim",
  {
    "folke/tokyonight.nvim",
    opts = {
      transparent = true,
      styles = {
        sidebars = "transparent",
        floats = "transparent",
      },
    },
    init = function()
      vim.cmd("colorscheme tokyonight")
    end,
  },
}
