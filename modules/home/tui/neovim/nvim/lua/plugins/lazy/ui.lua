return {
  "nvim-tree/nvim-web-devicons",
  {
    "iamcco/markdown-preview.nvim",
    build = "cd app & yarn install",
  },
  -- Bottom bar
  {
    "nvim-lualine/lualine.nvim",
    opts = {
      options = {
        theme = "horizon",
        -- theme = "gruvbox",
        disabled_filetypes = { "NvimTree" },
      },
      sections = {
        lualine_a = { "mode" },
        lualine_b = { "branch", "diff", "diagnostics" },
        lualine_c = { "searchcount", "selectioncount" },
        lualine_x = { "encoding", "fileformat", "filetype" },
        lualine_y = { "progress" },
        lualine_z = { "location" },
      },
      inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = {},
      },
    },
  },
  -- Top-most bar showing buffers
  {
    "akinsho/bufferline.nvim",
    dependencies = "nvim-tree/nvim-web-devicons",
    opts = {
      options = {
        show_tab_indicator = true,
        show_close_icon = false,
        show_buffer_close_icons = false,
        diagnostics = "nvim_lsp",
        offsets = {
          {
            filetype = "nvimtree",
            text = "file explorer",
            text_align = "center",
            separator = true,
          },
        },
        hover = {
          enable = true,
          reveal = { "close" },
        },
      },
    },
  },
  -- Winbar type, like you would see on VSCode and similar IDEs
  {
    "bekaboo/dropbar.nvim",
    dependencies = "nvim-telescope/telescope-fzf-native.nvim",
    opts = {
      bar = {
        enable = function(buf, win, _)
          return vim.api.nvim_buf_is_valid(buf)
            and vim.api.nvim_win_is_valid(win)
            and vim.wo[win].winbar == ""
            and vim.fn.win_gettype(win) == ""
            --and vim.bo[buf].ft ~= "help"
            --and ((pcall(vim.treesitter.get_parser, buf)) and true or false)
            and vim.bo[buf].buftype == "" -- Only show dropbar on real files
        end,
        hover = false,
        -- I have disabled all sources other than the file path.
        -- See https://github.com/Bekaboo/dropbar.nvim?tab=readme-ov-file#bar for the default options.
        sources = function(_, _)
          return { require("dropbar.sources").path }
        end,
      },
    },
    init = function()
      -- Fix an issue with dropbar background
      vim.api.nvim_set_hl(0, "WinBar", { bg = "NONE" })
    end,
  },
  {
    "folke/which-key.nvim",
  },
  {
    "akinsho/toggleterm.nvim",
    opts = {
      direction = "float",
    },
    init = function()
      -- Lazygit in vim
      local Terminal = require("toggleterm.terminal").Terminal
      local lazygit = Terminal:new({ cmd = "lazygit", hidden = true, dir = "git_dir" })

      local function lazygit_toggle()
        lazygit:toggle()
      end

      vim.keymap.set("n", "<leader>g", function()
        lazygit_toggle()
      end, { noremap = true, silent = true, desc = "Toggle Lazygit" })
      vim.keymap.set("n", "<C-Enter>", [[<Cmd>ToggleTerm<CR>]], { desc = "Toggle terminal" })
      vim.keymap.set("t", "<C-Enter>", [[<Cmd>ToggleTerm<CR>]], { desc = "Toggle terminal" })
    end,
  },
}
