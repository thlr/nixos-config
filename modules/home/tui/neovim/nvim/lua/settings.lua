-- general
vim.o.hidden = true
-- Copy things directly from and to the system clipboard, without having to pass through the selection register ('+', for instance: '"+y')
vim.o.clipboard = "unnamedplus"
vim.o.showcmd = true
vim.o.mouse = "a"
-- vim.o.wildmode = 'longest,list,full'
-- vim.o.colorcolumn = "80"

-- highlight trailing whitespace
vim.cmd("highlight ExtraWhitespace ctermbg=red guibg=red")
vim.cmd([[match ExtraWhitespace /\s\+\%#\@<!$/]])

-- Line wraps: soft-wrap test at 80 columns, don't hard-wrap text.
-- https://stackoverflow.com/a/50415982
--vim.o.textwidth = 0 -- hard-wrap option
--vim.o.wrapmargin = 0 -- margin from the *right* of the screen for soft-wrapping
--vim.o.wrap = true
vim.o.linebreak = true -- break by word rather than character

-- Disable default file explorer
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- numbers
vim.o.rnu = true
vim.o.nu = true

--timings
vim.o.timeoutlen = 800
vim.o.updatetime = 100

-- splits
vim.o.splitbelow = true
vim.o.splitright = true

-- folding
vim.o.foldmethod = "expr"
vim.o.foldlevel = 99

-- syntax highlighting
vim.o.syntax = "on"

-- title
vim.o.title = true
vim.o.titlestring = "%F"

-- indentation
vim.o.autoindent = true

-- theming
vim.o.termguicolors = true -- Enables 24-bit RGB color

-- LSP
vim.lsp.set_log_level("info")

-- Autocompletion
vim.o.completeopt = "menuone,menuone,noselect" -- not sure what this does anymore

-- Add Filetype detection for Helm templates (and Go templates)
vim.filetype.add({
  extension = {
    gotmpl = "gotmpl",
  },
  pattern = {
    [".*/templates/.*%.tpl"] = "helm",
    [".*/templates/.*%.ya?ml"] = "helm",
    ["helmfile.*%.ya?ml"] = "helm",
  },
})
