vim.keymap.set("n", "<Space>", "<NOP>")

vim.g.mapleader = " "
vim.g.maplocalleader = " "

local function toggle_autocomment()
  local current_fo = vim.opt_local.fo:get()
  if current_fo.c and current_fo.r and current_fo.o then
    vim.opt_local.fo:remove({ "c", "r", "o" })
  else
    vim.opt_local.fo:append("cro")
  end
end

vim.keymap.set("n", "<leader>c", toggle_autocomment, { desc = "Toggle autocomment" })
vim.keymap.set("n", "<leader>i", function()
  vim.bo.autoindent = not vim.bo.autoindent
end, { desc = "Toggle autoindent" })

-- TODO move this in a command palette
vim.keymap.set("n", "<leader>sfr", ":setlocal spell! spelllang=fr<CR>", { desc = "Set spellcheck language to fr" })
vim.keymap.set("n", "<leader>sen", ":setlocal spell! spelllang=en_us<CR>", { desc = "Set spellcheck language to en" })
