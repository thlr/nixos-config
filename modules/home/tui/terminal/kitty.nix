{ config, ... }:
let
  cfg = config.thlr;
in
{
  # Issue with SSH https://www.reddit.com/r/linux4noobs/comments/tkvs8o/kitty_terminal_with_ssh_issues/
  home.shellAliases.ssh = "kitty +kitten ssh";
  programs.kitty = {
    enable = true;

    shellIntegration.enableZshIntegration = true;

    # List of theme files Gruvbox Dark https://github.com/kovidgoyal/kitty-themes/tree/master/themes
    # Use `kitty +kitten themes` for a complete list
    # TODO parameterize
    #themeFile = "OneDark";
    #themeFile = "GruvboxMaterialDarkSoft";
    # theme = "Doom Vibrant";
    themeFile = "JetBrains_Darcula";
    # theme = "Atom";
    # theme = "citylights";

    settings = {
      enable_audio_bell = false;
      bell_on_tab = false;
      background_opacity = cfg.terminal.opacity;
      font_family = cfg.fontName;
      bold_font = "auto";
      italic_font = "auto";
      bold_italic_font = "auto";
      font_size = "15";
      confirm_os_window_close = 0;
      cursor_shape = "block";
    };
    keybindings = {
      "clear_all_shortcuts" = "yes";
      "alt+c" = "copy_to_clipboard";
      "alt+v" = "paste_from_clipboard";
      "alt+k" = "scroll_line_up";
      "alt+j" = "scroll_line_down";
      "alt+u" = "scroll_page_up";
      "alt+d" = "scroll_page_down";
      "alt+shift+k" = "change_font_size all +2.0";
      "alt+shift+j" = "change_font_size all -2.0";
      "super+shift+enter" = "launch --cwd=current --type=os-window";
    };
  };
}
