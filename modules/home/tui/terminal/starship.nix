{ lib, ... }:
{
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      add_newline = false;
      kubernetes = {
        disabled = false;
        style = "bold blue";
      };
      package.disabled = true;
      aws.disabled = true;
      python.symbol = " ";
      directory.truncate_to_repo = false;
      time.disabled = false;
    };
  };
}
