{ pkgs, config, ... }:
{

  home.shellAliases.lf = "lfcd";
  home.packages = with pkgs; [
    poppler_utils # pdftotext
    bat
    fzf
  ];

  programs = {
    lf = {
      enable = true;

      extraConfig = ''
        # Custom open command
        cmd open &{{
          case $(file --mime-type -Lb $f) in
            text/*) lf -remote "send $id \$$EDITOR \$fx";;
            *) for f in $fx; do $OPENER $f > /dev/null 2> /dev/null & done;;
          esac
        }}

        # Starship shell integration
        cmd on-cd &{{
            export STARSHIP_SHELL=
            fmt="$(starship prompt)"
            lf -remote "send $id set promptfmt \"$fmt\""
        }}

        # fzf integration
        cmd fzf_jump ''${{
          res="$(find . -type d | fzf --reverse --header='Jump to location')"
          if [ -n "$res" ]; then
              if [ -d "$res" ]; then
                  cmd="cd"
              else
                  cmd="select"
              fi
              res="$(printf '%s' "$res" | sed 's/\\/\\\\/g;s/"/\\"/g')"
              lf -remote "send $id $cmd \"$res\""
          fi
        }}
        map f :fzf_jump
      '';
      keybindings = {
        dd = "delete";
        R = "reload";
        "." = "set hidden!";
        o = "open";
        "<enter>" = "$SHELL";
        yy = "copy";

        # Common directories
        gd = "cd ${config.xdg.userDirs.download}";
      };
      settings = {
        ignorecase = true;
        drawbox = true;
        hidden = false;
      };
      previewer.source = pkgs.writeShellScript "lf-previewer.sh" ''
        #!/bin/sh
        file=$1
        case "$file" in
            *.tar*) tar tf "$file";;
            *.zip) unzip -l "$file";;
            *.rar) unrar l "$file";;
            *.pdf) pdftotext "$file" -;;
            *) bat --color=always --plain "$file";;
        esac
      '';
    };

    zsh.initExtra = ''
      # Change working dir in shell to last dir in lf on exit.
      lfcd () {
        tmp="$(mktemp)"
        lf -last-dir-path="$tmp" "$@"
        if [ -f "$tmp" ]; then
          dir="$(cat "$tmp")"
          rm -f "$tmp"
          if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
              cd "$dir"
            fi
          fi
        fi
      }
    '';
  };
}
