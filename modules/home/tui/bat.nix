{ pkgs, ... }:
{
  home = {
    shellAliases.cat = "bat -P --decorations never";
    packages = with pkgs; [ bat ];
  };
}
