{
  home = {
    shellAliases.nb = "newsboat";
  };

  programs.newsboat = {
    enable = true;
    extraConfig = ''
      # general settings
      auto-reload yes
      max-items 50

      # unbind keys
      unbind-key ENTER
      unbind-key j
      unbind-key k
      unbind-key J
      unbind-key K

      # bind keys - vim style
      bind-key j down
      bind-key k up
      bind-key l open
      bind-key h quit
      bind-key J next-feed articlelist
      bind-key K prev-feed articlelist

      # solarized
      color background         default   default
      color listnormal         default   default
      color listnormal_unread  default   default
      color listfocus          black     cyan
      color listfocus_unread   black     cyan
      color info               default   black
      color article            default   default

      # highlights
      highlight article "^(Title):.*$" blue default
      highlight article "https?://[^ ]+" red default
      highlight article "\\[image\\ [0-9]+\\]" green default
    '';
    urls = [
      # ARCHIVES
      # {
      #   url = "https://tidyfirst.substack.com/feed";
      #   title = "Tidy First";
      # }
      # {
      #   url = "https://martinfowler.com/feed.atom";
      #   title = "Martin Fowler";
      # }
      # {
      #   url = "http://feeds.kottke.org/main";
      #   title = "kottke";
      # }
      {
        url = "https://www.tweag.io/rss.xml";
        title = "Tweag";
      }
      {
        url = "https://tinyprojects.dev/feed.xml";
        title = "tinyprojects";
      }
      {
        url = "http://www.aaronsw.com/2002/feeds/pgessays.rss";
        title = "Paul Graham";
      }
      {
        url = "https://xeiaso.net/blog.rss";
        title = "Xe Iaso";
      }
      {
        url = "https://maggieappleton.com/rss.xml";
        title = "Maggie Appleton";
      }
      {
        url = "https://musicforprogramming.net/rss.xml";
        title = "Music for Programming";
      }
      {
        url = "https://solar.lowtechmagazine.com/posts/index.xml";
        title = "Low Tech Magazine";
      }
      {
        url = "https://simone.org/feed";
        title = "simone.org";
      }
      {
        url = "https://blog.ceejbot.com/index.xml";
        title = "Ceejbot's notes";
      }
      {
        url = "https://cerebralab.com/feed.xml";
        title = "cerebralab";
      }
      {
        url = "https://www.adamconrad.dev/rss.xml";
        title = "Adam Conrad";
      }
      {
        url = "https://awesomekling.github.io/feed.xml";
        title = "Andreas Kling";
      }
      {
        url = "https://obsidian.md/feed.xml";
        title = "Obsidian Blog";
      }
      {
        url = "https://jvns.ca/atom.xml";
        title = "Julia Evans";
      }
      {
        url = "https://factorio.com/blog/rss";
        title = "Factorio Blog";
      }
      {
        url = "https://brr.fyi/feed.xml";
        title = "brr";
      }
      {
        url = "https://about.gitlab.com/atom.xml";
        title = "Gitlab blog";
      }
      {
        url = "https://nixos.org/blog/announcements-rss.xml";
        title = "NixOS anouncements";
      }
      {
        url = "https://xkcd.com/rss.xml";
        title = "XKCD";
      }
      {
        url = "https://neovim.io/news.xml";
        title = "Neovim";
      }
      {
        url = "https://kubernetes.io/feed.xml";
        title = "Kubernetes blog";
      }
      {
        url = "https://jcarlosroldan.com/rss";
        title = "J. Carlos Roldán";
      }
      {
        url = "https://laplab.me/posts/index.xml";
        title = "Nikita Lapkov";
      }
      {
        url = "https://stephango.com/feed.xml";
        title = "Steph \"Kepano\" Ango (Obsidian)";
      }
      {
        url = "https://lessfoolish.substack.com/feed";
        title = "Less Foolish";
      }
      {
        url = "https://ciechanow.ski/atom.xml";
        title = "Ciechanow";
      }
      {
        url = "https://serverlesshorrors.com/rss.xml";
        title = "Serverless Horror Stories";
      }
      {
        url = "https://ludic.mataroa.blog/rss/";
        title = "Ludicity";
      }
      {
        url = "https://iximiuz.com/feed.rss";
        title = "Ivan Velichko";
      }
      {
        url = "https://www.spakhm.com/feed.rss";
        title = "Slava Akhmechet";
      }
      {
        url = "https://maheshba.bitbucket.io/blog/feed.xml";
        title = "Mahesh's blog";
      }
      {
        url = "https://thecodist.com/rss/";
        title = "The Codist";
      }
      {
        url = "https://surfingcomplexity.blog/feed";
        title = "Surfing Complexity";
      }
      {
        url = "https://charleshughsmith.substack.com/feed";
        title = "Charles Hugh Smith";
      }
      {
        url = "https://progressandpoverty.substack.com/feed";
        title = "Progress and Poverty";
      }
    ];
  };
}
