{ pkgs, ... }:
{
  home = {
    shellAliases.z = "zathura";
  };

  # Cheatsheet: https://diepfote.github.io/cheatsheets/zathura/bindings/
  # Reference: https://manpages.ubuntu.com/manpages/trusty/man5/zathurarc.5.html

  programs.zathura = {
    enable = true;
    package = pkgs.unstable.zathura;
    options = {
      selection-clipboard = "clipboard";
      adjust-open = "width";
      database = "sqlite";
      guioptions = "s";
      smooth-scroll = true;
      scroll-full-overlap = "0.01";
      scroll-step = 100;
    };
  };
}
