{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  home.file.".profile".text = ''
    # Basics
    export BROWSER=firefox
    export EDITOR=nvim
    export TERMINAL=${cfg.terminal.emulator}

    # Put user binaries in front so they don't get overwritten by system packages
    export PATH="/etc/profiles/per-user/tlarue/bin:$PATH"
  '';

  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
    defaultKeymap = "viins";
    dirHashes = {
      dl = "$HOME/Downloads";
    };
    plugins = [
      {
        name = "zsh-vi-mode";
        src = pkgs.zsh-vi-mode;
        file = "share/zsh-vi-mode/zsh-vi-mode.plugin.zsh";
      }
    ];
    envExtra = ''
      source $HOME/.profile
    '';
    initExtra = concatStrings (
      [
        ''
          # Key bindings
          bindkey "^R" history-incremental-search-backward
          bindkey "^[[1;5C" forward-word
          bindkey "^[[1;5D" backward-word

          # created by poetry for autocompletion
          autoload -Uz compinit
          zstyle ':completion:*' menu select
          fpath+=~/.zfunc
        ''
      ]
      ++ optionals cfg.work.enable [
        ''
          if [ "$(command -v kubectl)" ]; then
            [ -f ~/.config/kubectl_aliases ] && source ~/.config/kubectl_aliases

            # kubectl and minikube autocompletion
            source <(kubectl completion zsh)
          fi
        ''
      ]
    );
  };
}
