{ pkgs, ... }:
{

  imports = [
    ./neovim
    ./terminal
    ./bat.nix
    ./eza.nix
    ./lf.nix
    ./newsboat.nix
    ./python.nix
    ./ripgrep.nix
    ./zathura.nix
    ./zsh.nix
  ];

  services = {
    udiskie.enable = true;
  };

  home = {
    shellAliases = {
      du = "dua";
      calc = "gnome-calculator";
      # The following allows to use watch with other aliases.
      # Explanation: https://unix.stackexchange.com/questions/25327/watch-command-alias-expansion
      watch = "watch ";
    };

    packages = with pkgs; [
      # Basic CLI utilities
      btop
      fd
      unzip
      dua
      tree
      file
      killall
      usbutils
      moreutils
      wget
      openssl
      lsof
      envsubst

      # Other CLI tools
      yq-go # https://github.com/mikefarah/yq
      jq
      mkpasswd
      neofetch
      ncdu
    ];
  };
}
