{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf cfg.hyprland.enable {
  thlr.wayland.enable = true;
  programs.hyprland = {
    enable = true;
    #package = pkgs.unstable.hyprland;
    #xwayland.enable = true;
  };
  security.polkit.enable = true;
}
