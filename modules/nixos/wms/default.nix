{
  imports = [
    ./gnome.nix
    ./hyprland.nix
    ./qtile.nix
  ];
}
