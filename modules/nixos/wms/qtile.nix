{
  lib,
  config,
  username,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
{
  config = mkIf cfg.qtile.enable {
    thlr.xorg.enable = true;
    services = {
      displayManager.defaultSession = "qtile";
      displayManager.environment = {
        # For user scripts
        "HOME" = "${config.users.users.${username}.home}";
      };
      xserver = {
        windowManager.qtile = {
          enable = true;
        };
        displayManager = {
          lightdm.enable = true;
        };
      };
    };
  };
}
