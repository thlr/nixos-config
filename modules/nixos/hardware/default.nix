{
  imports = [
    ./pipewire.nix
    ./printing.nix
    ./power.nix
  ];
}
