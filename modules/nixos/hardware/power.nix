{
  powerManagement.cpuFreqGovernor = "powersave";
  services = {
    power-profiles-daemon.enable = true;
    thermald.enable = true; # For intel
  };
}
