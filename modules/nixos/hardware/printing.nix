{
  services = {
    # Printers are configured in localhost:631
    printing.enable = true;

    # Automatically detect recent network printers
    avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };
  };

}
