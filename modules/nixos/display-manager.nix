{
  lib,
  config,
  pkgs,
  username,
  ...
}:
with lib;
let
  cfg = config.thlr;
  regreetHyprlandConfigTarget = "greetd/hyprland-config";
in
mkIf cfg.hyprland.enable {
  services.greetd = {
    enable = true;
    settings = {
      default_session = {
        command = "${pkgs.hyprland}/bin/Hyprland --config /etc/${regreetHyprlandConfigTarget}";
        user = "greeter";
      };
      initial_session = mkIf cfg.hyprland.autoLogin {
        command = "${pkgs.hyprland}/bin/Hyprland";
        user = username;
      };
    };
  };
  environment.etc.regreetHyprlandConfig = {
    target = regreetHyprlandConfigTarget;
    text = ''
      exec = sh -c "${pkgs.greetd.regreet}/bin/regreet; ${pkgs.hyprland}/bin/hyprctl dispatch exit"
      misc {
          disable_hyprland_logo = true
          disable_splash_rendering = true
      }
      input {
          kb_layout = fr
          kb_options = caps:escape
      }
      monitor = ${cfg.hyprland.internalMonitorIdentifier},preferred,0x0,1
      monitor = ,preferred,auto,1
    '';
  };
  # To test configuration, run a shell with the latest regreet
  # nix-shell -I nixpkgs=channel:nixos-unstable -p greetd.regreet
  # And test with its demo mode
  # regreet --demo
  programs.regreet = {
    enable = true;
    settings = {
      background = {
        path = ../../pics/nixos.png;
        fit = "Fill";
      };
    };
  };
}
