{ lib, pkgs, ... }:
with lib;
{
  imports = [
    ../options.nix
    ./hardware
    ./wms
    ./display.nix
    ./display-manager.nix
    ./wallpaper-daemon.nix
  ];

  config = {
    system = {
      stateVersion = "23.05";
      activationScripts.diff = {
        supportsDryActivation = true;
        text = ''
          ${pkgs.nvd}/bin/nvd --nix-bin-dir=${pkgs.nix}/bin diff /run/current-system "$systemConfig"
        '';
      };
      # This allows to entirely patch and rebuild the system offline but it drastically increases closure size. Except an increase from 8 to 10 times larger.
      includeBuildDependencies = false;
    };

    environment = {
      localBinInPath = true;
      systemPackages = with pkgs; [
        neovim
        git
        nvd # Makes nice diffs when upgrading nix flake
      ];
    };

    nix = {
      # optimise nix store for newer derivations
      settings.auto-optimise-store = true;

      # Enable Flakes and disable garbage collection for direnv
      extraOptions = ''
        experimental-features = nix-command flakes
        keep-outputs = true
        keep-derivations = true
      '';
    };

    services = {
      # Limit journald size
      journald.extraConfig = "SystemMaxUse=1G";

      # Power button and lid close behavior
      logind = {
        extraConfig = "HandlePowerKey=suspend";
        lidSwitch = "suspend";
      };

      # Bluetooth
      blueman.enable = true;

      # usb dbus
      udisks2.enable = true;

      # printer support
      printing.enable = true;
    };

    # Supposedly better for the SSD
    fileSystems."/".options = [
      "noatime"
      "nodiratime"
      "discard"
    ];

    i18n.defaultLocale = "fr_FR.UTF-8";

    networking.networkmanager.enable = true;
    time.timeZone = "Europe/Paris";
    console = {
      font = "Lat2-Terminus16";
      useXkbConfig = true; # use xkbOptions in tty.
      keyMap = lib.mkForce "fr";
    };

    hardware = {
      enableRedistributableFirmware = true;

      # https://nixos.wiki/wiki/Bluetooth
      bluetooth = {
        enable = true;
        settings = {
          General = {
            Enable = "Source,Sink,Media,Socket";
          };
        };
      };
    };

    programs = {
      dconf.enable = true; # For GTK themes and also easyeffects equalizer
      zsh.enable = true;
      ssh.startAgent = true;
    };

    # Enable the OpenSSH daemon.
    services.openssh.enable = true;

    fonts.packages = with pkgs; [
      (nerdfonts.override {
        fonts = [
          "Cousine"
          "JetBrainsMono"
          "Ubuntu"
        ];
      })
    ];
  };
}
