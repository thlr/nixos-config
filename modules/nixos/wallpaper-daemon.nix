{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkIf (cfg.utilities.wallpaperEngine == "swww") {
  systemd.user.services.swww = {
    enable = true;
    wantedBy = [ "graphical-session.target" ];
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.swww}/bin/swww-daemon";
    };
  };
}
