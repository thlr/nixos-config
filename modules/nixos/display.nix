{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.thlr;
in
mkMerge [
  ({
    services.libinput = {
      enable = true; # enable touchpad
      mouse = {
        accelProfile = "flat";
        # https://askubuntu.com/questions/172972/configure-mouse-speed-not-pointer-acceleration
        transformationMatrix = "${cfg.mouseSpeed} 0 0 0 ${cfg.mouseSpeed} 0 0 0 1";
      };
      touchpad = {
        naturalScrolling = true;
        disableWhileTyping = true;
      };
    };
  })

  (mkIf cfg.xorg.enable {
    services.xserver = {
      enable = true;
      videoDrivers = [
        "intel"
        "modsetting"
      ];
      xkb.layout = "fr";
    };
    # services.compton is just an alias of services.picom
    services.compton = {
      enable = true;
      vSync = true;
      shadow = false;
    };
  })

  (mkIf cfg.wayland.enable {
    environment.sessionVariables = {
      NIXOS_OZONE_WL = "1";
    };

    services.dbus.enable = true;
  })
]
